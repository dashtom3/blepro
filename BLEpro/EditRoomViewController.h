//
//  EditRoomViewController.h
//  BLEpro
//
//  Created by u on 14-3-4.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FurtherEditRoomViewController.h"

@interface EditRoomViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *rooms;

@property(strong,nonatomic) ModelAnalyse *analyse;
@property (weak,nonatomic)FurtherEditRoomViewController *fervc;

@end
