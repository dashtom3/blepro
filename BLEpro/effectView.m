//
//  effectView.m
//  BLEpro
//
//  Created by u on 14-5-30.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "effectView.h"

@implementation effectView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _ma = [[ModelAnalyse alloc]init];
        _connectedlights = [[NSMutableArray alloc]init];
        _control = [[NSMutableDictionary alloc]init];
        [self refresh];
    }
    return self;
}
- (IBAction)lightesswitchchange:(id)sender {
    if([[_control valueForKey:@"lrcontrol"]isEqualToString:@"1"]){
        [_control setObject:@"0" forKey:@"lrcontrol"];
        [_ma changeControl:_control];
    }else{
        [_control setObject:@"1" forKey:@"lrcontrol"];
        [_ma changeControl:_control];
    }
}
- (IBAction)pushViewController:(id)sender {
    if ([_delegate respondsToSelector:@selector(pushEffectShakeViewController)]) {
        [_delegate pushEffectShakeViewController];
    }
}
-(void)refresh{
    _ma = [[ModelAnalyse alloc]init];
    _connectedlights = [[NSMutableArray alloc]init];
    _connectedlights = [_ma getAllConnectedLights];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"lrstate==%@",@"1"];
    ;
    _lightesnum.text=[NSString stringWithFormat:@"%lu  %@",(unsigned long)[_connectedlights filteredArrayUsingPredicate:predicate].count,NSLocalizedString(@"lrnum", nil)];
    
    _control = [_ma getAllControl];
    if([[_control valueForKey:@"lrcontrol"]isEqualToString:@"1"]){
        _lightesswitch.on = YES;
    }
    else {
        _lightesswitch.on = NO;
    }
}
@end
