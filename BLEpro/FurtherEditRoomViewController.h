//
//  FurtherEditRoomViewController.h
//  BLEpro
//
//  Created by u on 14-3-5.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room.h"
#import "RoomTextViewController.h"
#import "alllightsCell.h"
#import "ModelAnalyse.h"
#import "RoomAddLightViewController.h"
enum{
    //编辑房间,新建房间
    newRoom=0,
    editRoom,
};

@interface FurtherEditRoomViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *rname;
@property (nonatomic)NSMutableDictionary *room;
@property(strong,nonatomic) NSMutableArray *lights;
@property(strong,nonatomic) NSMutableArray *selectedlights;
@property (nonatomic) int state; // 状态0 添加房间 状态1 修改房间
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) UILabel *t;
@property (weak,nonatomic)RoomAddLightViewController *ralvc;
@property (weak,nonatomic)RoomTextViewController *rtvc;
@end
