//
//  LightBreathViewController.m
//  BLEpro
//
//  Created by u on 14-5-22.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightBreathViewController.h"
#import "BLEproAppDelegate.h"
@interface LightBreathViewController (){
    BLEproAppDelegate *blead;
}

@end

@implementation LightBreathViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _ma=[[ModelAnalyse alloc]init];
    _lp= [[LightProvider alloc]init];
    _bp=[_lp searchBLEfromLight:_light];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", nil)
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    
}
- (void) handleBack:(id)sender
{
    // pop to root view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self refresh];
}
-(void)refresh{
    _timer=[_ma getTimeOfLight:_light];
    _room= [_ma getRoom:[_light valueForKey:@"lroom"]];
    _roomname.text = [_room valueForKey:@"rname"];
    _lightname.text = [_light valueForKey:@"lname"];
}
- (IBAction)closeBreath:(id)sender {
    [_light setObject:@"0" forKey:@"lbreathstate"];
    [_ma changeLight:_light];
    [_bp breathClose];
    [_bp openTime:_timer Wake:_light];
    [_bp methodOpenWithTime:_timer WithLight:_light];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
