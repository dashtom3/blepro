//
//  listView.h
//  BLEpro
//
//  Created by u on 14-5-19.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLEproAppDelegate.h"
#import "ModelAnalyse.h"
#import "listLightView.h"

@interface listView : UIView
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) listLightView *lev;
@property (nonatomic) NSMutableArray *rooms;
@property (nonatomic) UILabel *roomName;
@property (nonatomic) int height;
@property id delegate;

-(void)refresh;
@end
