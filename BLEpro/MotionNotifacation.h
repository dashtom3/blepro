//
//  MotionNotifacation.h
//  BLEpro
//
//  Created by u on 14-5-11.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelAnalyse.h"
#import "LightProvider.h"
#import "DateAnalyse.h"
@class LightProvider;
enum{
    shakeChange=0,
    
};
@interface MotionNotifacation : NSObject
@property (nonatomic)ModelAnalyse *analyse;
@property (nonatomic) NSMutableDictionary *control;
@property (nonatomic) NSMutableArray *lights;
@property (nonatomic) LightProvider *lp;
@property (nonatomic)NSMutableArray *timer;
@property (nonatomic)DateAnalyse *da;
@end
