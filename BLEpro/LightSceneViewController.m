//
//  LightSceneViewController.m
//  BLEpro
//
//  Created by u on 14-4-23.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightSceneViewController.h"
#import "BLEproAppDelegate.h"
#import "UIColor-Expanded.h"
@interface LightSceneViewController (){
    BLEproAppDelegate   *blead;
}


@end

@implementation LightSceneViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    blead = [[UIApplication sharedApplication]delegate];

    [self.view setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)64/255 alpha:1.0]];
    _t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    _t.font = [UIFont systemFontOfSize:19];
    _t.textColor = [UIColor whiteColor];
    _t.backgroundColor = [UIColor clearColor];
    _t.textAlignment = NSTextAlignmentCenter;
    _t.text = [_light valueForKey:@"lname"];
    self.navigationItem.titleView = _t;
    
    UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [rightButton setImage:[UIImage imageNamed:@"edit.png"]forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(editlight)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
    
    _analyse = [[ModelAnalyse alloc]init];
    _lp = [[LightProvider alloc]init];
    _da = [[DateAnalyse alloc]init];
    _bp = [_lp searchBLEfromLight:_light];
    _timer = [[NSMutableArray alloc]init];
    _control = [[NSMutableDictionary alloc]init];
    
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [self refresh];
}
-(void)refresh{
    _control = [_analyse getAllControl];
    _light =[_analyse getLight:[_light valueForKey:@"lnameID"]];
    _timer = [_analyse getTimeOfLight:_light];
    //灯信息
    _t.text = [_light valueForKey:@"lname"];
    _lName.text = [_light valueForKey:@"lname"];
    if([[_light valueForKey:@"lroom"]isEqualToString:@"无"]){
        _lRoom.text = NSLocalizedString(@"no", nil);
    }else {
        _room = [[NSMutableDictionary alloc]init];
        _room = [_analyse getRoom:[_light valueForKey:@"lroom"]];
        _lRoom.text = [_room valueForKey:@"rname"];
    }
    if([[_light valueForKey:@"lstate"]isEqualToString:@"0"]){
        [_lState setBackgroundImage:[UIImage imageNamed:@"loff.png"]forState:UIControlStateNormal];
    }else if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
        [_lState setBackgroundImage:[UIImage imageNamed:@"lon.png"]forState:UIControlStateNormal];
    }
    //灯操作信息
    //左边渐开按钮
    _lgbtn =[[UIButton alloc]initWithFrame:CGRectMake(0, 147, 56, 59)];
    [_lgbtn setBackgroundColor:[UIColor colorWithRed:(float)87/255 green:(float)96/255 blue:(float)119/255 alpha:1]];
    [_lgbtn addTarget:self action:@selector(lgradMotion)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_lgbtn];
    //右边开灯按钮
    _libtn =[[UIButton alloc]initWithFrame:CGRectMake(59,147,261,59)];
    [_libtn setBackgroundColor:[UIColor colorWithRed:(float)87/255 green:(float)96/255 blue:(float)119/255 alpha:1]];
    [self.view addSubview:_libtn];
    [_libtn addTarget:self action:@selector(lonoff)forControlEvents:UIControlEventTouchUpInside];
    //开启关闭按钮
    _lgradOn =[[UIButton alloc]initWithFrame:CGRectMake(90, 159, 51, 32)];
    _lgradOn.alpha=0.0;
    [_lgradOn setTitle:NSLocalizedString(@"oN", nil) forState:UIControlStateNormal];
    [_lgradOn addTarget:self action:@selector(lGradOn)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_lgradOn];
    
    _lgradOff = [[UIButton alloc]initWithFrame:CGRectMake(159, 159, 51, 32)];
    _lgradOff.alpha=0.0;
    [_lgradOff setTitle:NSLocalizedString(@"oFF", nil) forState:UIControlStateNormal];
    [_lgradOff addTarget:self action:@selector(lGradOff)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_lgradOff];
    //右边开启关闭图标
    [self lOnOffChange];
    //亮度调节
    _lBright.value = [[_light valueForKey:@"lbright"]intValue];
    if([[_light valueForKey:@"lstate"]isEqualToString:@"0"]){
        _lBright.enabled=NO;
        _lColorLabel.textColor= [UIColor grayColor];
    }
    _lColor.layer.cornerRadius = 6;
    _lColor.layer.masksToBounds = YES;
    [_lColor setBackgroundColor: [UIColor colorWithHexString:[_light valueForKey:@"lcolor"]]];
    if([[_light valueForKey:@"lwakestate"]isEqualToString:@"0"]){
        _lWakeState.text = NSLocalizedString(@"oFF", nil);
    }else if([[_light valueForKey:@"lwakestate"]isEqualToString:@"1"]){
        _lWakeState.text = NSLocalizedString(@"oN", nil);
    }
    [self lgradChange];
    NSString *a;
    NSString *b;
    a=NSLocalizedString(@"oN", nil);
    b=NSLocalizedString(@"oFF", nil);
    for(int i=0;i<_timer.count;i++){
        if([[[_timer objectAtIndex:i]valueForKey:@"tstate"]isEqualToString:@"1"]){
            a=[NSString stringWithFormat:@"%@ %@",a,[_da getHHmmfromdate:[[_timer objectAtIndex:i]valueForKey:@"tdate"]]];
        }else if([[[_timer objectAtIndex:i]valueForKey:@"tstate"]isEqualToString:@"0"]){
            b=[NSString stringWithFormat:@"%@ %@",b,[_da getHHmmfromdate:[[_timer objectAtIndex:i]valueForKey:@"tdate"]]];
        }
    }
    if([a isEqualToString:NSLocalizedString(@"oN", nil)]){
        a=NSLocalizedString(@"noSet", nil);
    }
    if([b isEqualToString:NSLocalizedString(@"oFF", nil)]){
        b=NSLocalizedString(@"noSet", nil);
    }
    _timerOn.text=a;
    _timerOff.text=b;
    if([[_light valueForKey:@"lwakestate"]isEqualToString:@"1"]){
        _lWakeState.text=[_da getHHmmfromdate:[_light valueForKey:@"lwaketime"]];
    }else{
        _lWakeState.text= NSLocalizedString(@"noSet", nil);
    }
}
-(void)lrefresh{
    if(self.view.window!=nil){
        _light = [_analyse getLight:[_light valueForKey:@"lnameID"]];
        _bp = [_lp searchBLEfromLight:_light];
        if(_bp.activePeripheral.state==0 ||_bp==nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"loseConnection",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok",nil)  otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        [_light setObject: [_light valueForKey:@"lstate"] forKey:@"lstate"];
        if([[_light valueForKey:@"lstate"]isEqualToString:@"0"]){
            [_lState setBackgroundImage:[UIImage imageNamed:@"loff.png"]forState:UIControlStateNormal];
        }else if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
            [_lState setBackgroundImage:[UIImage imageNamed:@"lon.png"]forState:UIControlStateNormal];
        }
        [self lOnOffChange];
    }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)lgradMotion {
    [self labelchange:1];
}
-(void)labelchange:(int)judge{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    if(judge==1){
        [UIView setAnimationDidStopSelector:@selector(showButton)];
        _lgbtn.frame = CGRectMake(_lgbtn.frame.origin.x, _lgbtn.frame.origin.y, 261, _lgbtn.frame.size.height);
        _libtn.frame = CGRectMake(263, 147,56,59);
    }else if(judge==0){
        [UIView setAnimationWillStartSelector:@selector(hideButton)];
        _lgbtn.frame = CGRectMake(_lgbtn.frame.origin.x, _lgbtn.frame.origin.y, 56, _lgbtn.frame.size.height);
        _libtn.frame = CGRectMake(59, _libtn.frame.origin.y,261,_libtn.frame.size.height);
    }
    [UIView commitAnimations];
}
-(void)showButton{
    _lgradOn.alpha =1.0;
    _lgradOff.alpha =1.0;
}
-(void)hideButton{
    _lgradOn.alpha =0;
    _lgradOff.alpha =0;
}
-(void)lOnOffChange{
    [_onlabel removeFromSuperview];
    [_onimage removeFromSuperview];
    _onlabel = [[UILabel alloc]initWithFrame:CGRectMake(130,10,70,40)];
    _onlabel.font = [UIFont systemFontOfSize:20];
    _onlabel.textColor=[UIColor whiteColor];
    if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
        _onlabel.text =NSLocalizedString(@"oN", nil);
        _onimage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"on1.png"]];
        _onimage.frame=CGRectMake(94,19,19,19);
        _lBright.enabled=YES;
        _lColorLabel.textColor= [UIColor whiteColor];
        
    }else if([[_light valueForKey:@"lstate"]isEqualToString:@"0"]){
        _onlabel.text =NSLocalizedString(@"oFF", nil);
        _onimage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"off1.png"]];
        _onimage.frame=CGRectMake(94,19,19,19);
        _lBright.enabled=NO;
        _lColorLabel.textColor= [UIColor grayColor];
    }
    [_libtn addSubview:_onlabel];
    [_libtn addSubview:_onimage];
}
-(void)lgradChange{
    _lgradState = [[UILabel alloc]initWithFrame:CGRectMake(10,31,45,14)];
    _lgradState.text =NSLocalizedString(@"grad", nil);
    _lgradState.font = [UIFont systemFontOfSize:10];
    if([[_light valueForKey:@"lgradstate"]isEqualToString:@"1"]){
        [_lgradpic removeFromSuperview];
        _lgradpic=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"lgon.png"]];
        _lgradpic.frame=CGRectMake(15,5,26,24);
        _lgradState.textColor=[UIColor whiteColor];
    }else if([[_light valueForKey:@"lgradstate"]isEqualToString:@"0"]){
        [_lgradpic removeFromSuperview];
        _lgradpic=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"lgoff.png"]];
        _lgradpic.frame=CGRectMake(14,5,28,26);
        _lgradState.textColor=[UIColor grayColor];
    }
    [_lgbtn addSubview: _lgradState];
    [_lgbtn addSubview:_lgradpic];
}
- (void)lGradOn{
    [_light setObject:@"1" forKey:@"lgradstate"];
    [_analyse changeLight:_light];
    [self lgradChange];
    [self labelchange:0];
}
- (void)lGradOff{
    [_light setObject:@"0" forKey:@"lgradstate"];
    [_analyse changeLight:_light];
    [self lgradChange];
    [self labelchange:0];
}
-(void)lonoff{
    if([[_light valueForKey:@"lstate"]isEqualToString:@"0"]){
        [_lState setBackgroundImage:[UIImage imageNamed:@"lon.png"]forState:UIControlStateNormal];
        [_light setObject:@"1" forKey:@"lstate"];
        if([[_light valueForKey:@"lgradstate"]isEqualToString:@"1"]){
            [_bp GradualClickOn: [_light valueForKey:@"lcolor"] WithAlpha:(int)_lBright.value];
        }else{
            [_bp InstClickOn: [_light valueForKey:@"lcolor"]WithAlpha:(int)_lBright.value];
        }
        
    }
    else if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
        [_lState setBackgroundImage:[UIImage imageNamed:@"loff.png"]forState:UIControlStateNormal];
        [_light setObject:@"0" forKey:@"lstate"];
        if([[_light valueForKey:@"lgradstate"]isEqualToString:@"1"]){
            [_bp GradualClickOff: [_light valueForKey:@"lcolor"]WithAlpha:(int)_lBright.value];
            [self methodOpen];
        }else{
            [_bp InstClickOff];
        }
        
    }
    [_analyse changeLight:_light];
    [self lOnOffChange];
}
- (IBAction)lstatechange:(id)sender {
    [self lonoff];
}
- (IBAction)lBrightChange:(UISlider *)sender {
    if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
        int colo=[[_light valueForKey:@"lbright"]intValue];
        int pInt=(int)_lBright.value;
        if(fabs(colo-pInt)>25){
            colo=pInt;
            if (pInt<15) {
                colo=0;
            }
            else if(pInt>240){
                colo=255;
            }
            [_light setObject:[NSString stringWithFormat:@"%d",colo] forKey:@"lbright"] ;
            [_analyse changeLight:_light];
            [_bp InstClickOn:[_light valueForKey:@"lcolor"] WithAlpha:_lBright.value];
        }
    }else{
        [_light setObject:[NSString stringWithFormat:@"%d",(int)_lBright.value] forKey:@"lbright"];
        [_analyse changeLight:_light];
    }
}

-(void)editlight{
    _felvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightDetail"];
    _felvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _felvc.light =[[NSMutableDictionary alloc]init];
    _felvc.light = _light;
    [self.navigationController pushViewController:_felvc animated:YES];
}
- (IBAction)pushColorChangeVC:(id)sender {
    if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
        _lcpvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightColor"];
        _lcpvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _lcpvc.light = [[NSMutableDictionary alloc]init];
        _lcpvc.light = _light;
        [self.navigationController pushViewController:_lcpvc animated:YES];
    }
}
- (IBAction)operateSleep:(id)sender {
    [_bp sleep];
    [self methodOpen];
}
- (IBAction)pushBreathVC:(id)sender {
        [_bp breathOpen];
        [_light setObject:@"1" forKey:@"lbreathstate"];
        [self methodOpenB];
        [_analyse changeLight:_light];
        _lbvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lbreath"];
        _lbvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _lbvc.light = [[NSMutableDictionary alloc]init];
        _lbvc.light = _light;
        [self.navigationController pushViewController:_lbvc animated:YES];
}

- (IBAction)pushTimeSetVC:(id)sender {
    _ltvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ltime"];
    _ltvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _ltvc.light =[[NSMutableDictionary alloc]init];
    _ltvc.light = _light;
    _ltvc.state=0;
    [self.navigationController pushViewController:_ltvc animated:YES];
}
- (IBAction)pushWakeSetVC:(id)sender {
    _ltvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lwake"];
    _ltvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _ltvc.light =[[NSMutableDictionary alloc]init];
    _ltvc.light = _light;
    _ltvc.state=1;
    [self.navigationController pushViewController:_ltvc animated:YES];
}
-(void)methodOpen{
    [_bp methodOpenWithTime:_timer WithLight:_light];
}
-(void)methodOpenB{
    [_bp methodOpenWithBreath:_light];
}
@end
