//
//  Light.h
//  BLEpro
//
//  Created by u on 14-5-21.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Light : NSManagedObject

@property (nonatomic, retain) NSString * lbreathstate;
@property (nonatomic, retain) NSString * lcolor;
@property (nonatomic, retain) NSString * lconfig;
@property (nonatomic, retain) NSString * lconnect;
@property (nonatomic, retain) NSString * lgradstate;
@property (nonatomic, retain) NSString * lID;
@property (nonatomic, retain) NSString * lname;
@property (nonatomic, retain) NSString * lnameID;
@property (nonatomic, retain) NSString * lroom;
@property (nonatomic, retain) NSString * lrstate;
@property (nonatomic, retain) NSString * lstate;
@property (nonatomic, retain) NSString * lwakestate;
@property (nonatomic, retain) NSDate * lwaketime;
@property (nonatomic, retain) NSString * lbright;

@end
