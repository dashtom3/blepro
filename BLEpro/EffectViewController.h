//
//  EffectViewController.h
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "slideView.h"
#import "ModelAnalyse.h"
#import "lightsEffectView.h"
#import "LightViewController.h"
#import "RoomViewController.h"
#import "LightProvider.h"
#import "ShakeEffectViewController.h"


@class LightViewController;
@class RoomViewController;
@interface EffectViewController : UIViewController



@property(nonatomic) ModelAnalyse *analyse;
@property (nonatomic) NSMutableArray *lights;
@property (strong,nonatomic) slideView *uiView;
@property(nonatomic) int state;


@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak,nonatomic) ShakeEffectViewController *sevc;
@property (weak, nonatomic) IBOutlet UISwitch *lcontrol;
@property (weak, nonatomic) IBOutlet UILabel *lnum;
@property (nonatomic) NSMutableDictionary *control;
@property (nonatomic) LightProvider *lp;
@property (weak,nonatomic) LightViewController *lvc;
@property (weak,nonatomic) RoomViewController *rvc;
@end
