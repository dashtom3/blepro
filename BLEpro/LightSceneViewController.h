//
//  LightSceneViewController.h
//  BLEpro
//
//  Created by u on 14-4-23.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LightProvider.h"
#import "ModelAnalyse.h"
#import "timeUpView.h"
#import "DateAnalyse.h"
#import "blePeripheral.h"
#import "FurtherEditLightViewController.h"
#import "LightColorPickViewController.h"
#import "LightTimeViewController.h"
#import "LightBreathViewController.h"
#import "alertView.h"
@interface LightSceneViewController : UIViewController
@property (nonatomic) UIButton *lgradOff;
@property (nonatomic) UIButton *lgradOn;
@property (weak, nonatomic) IBOutlet UILabel *lName;
@property (weak, nonatomic) IBOutlet UILabel *lRoom;
@property (weak, nonatomic) IBOutlet UIButton *lState;
@property (nonatomic) UIButton *lgbtn;
@property (nonatomic) UILabel *lgradState;
@property (nonatomic) UIButton *libtn;
@property (nonatomic) UILabel *onlabel;
@property (nonatomic) UIImageView *onimage;
@property (weak, nonatomic) IBOutlet UISlider *lBright;
@property (strong, nonatomic) IBOutlet UILabel *lColorLabel;
@property (weak, nonatomic) IBOutlet UIButton *lColor;
@property (nonatomic) UIView *avv;
@property (weak, nonatomic) IBOutlet UILabel *timerOn;
@property (weak, nonatomic) IBOutlet UILabel *timerOff;
@property (weak, nonatomic) IBOutlet UILabel *lWakeState;
@property (nonatomic) UIImageView *lgradpic;
@property (nonatomic)NSMutableDictionary *light;
@property (nonatomic)NSMutableDictionary *room;
@property (nonatomic) NSMutableDictionary *control;
@property (nonatomic) ModelAnalyse *analyse;
@property (nonatomic)LightProvider *lp;
@property (nonatomic) DateAnalyse *da;
@property (nonatomic) blePeripheral *bp;
@property (nonatomic) UILabel *t;
@property (weak,nonatomic)FurtherEditLightViewController *felvc;
@property (weak,nonatomic)LightColorPickViewController *lcpvc;
@property (weak,nonatomic)LightTimeViewController *ltvc;
@property (weak,nonatomic)LightBreathViewController *lbvc;
@property (nonatomic) NSMutableArray *timer;
@end
