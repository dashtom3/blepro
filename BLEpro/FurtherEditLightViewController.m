//
//  FurtherEditLightViewController.m
//  BLEpro
//
//  Created by u on 14-2-22.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "FurtherEditLightViewController.h"
#import "LightColorPickViewController.h"

#import "UIColor-Expanded.h"
#import "LightTextViewController.h"
#import <CoreData/CoreData.h>
@interface FurtherEditLightViewController (){
    BLEproAppDelegate *blead;
}



@end

@implementation FurtherEditLightViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    blead = [[UIApplication sharedApplication]delegate];
    
    _lp = [[LightProvider alloc]init];
    _analyse = [[ModelAnalyse alloc]init];
    _rooms = [[NSMutableArray alloc]init];
    _lightName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    _roomname.titleLabel.font = [UIFont systemFontOfSize:20];
    _t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    _t.font = [UIFont systemFontOfSize:19];
    _t.textColor = [UIColor whiteColor];
    _t.backgroundColor = [UIColor clearColor];
    _t.textAlignment = NSTextAlignmentCenter;
    _t.text = [_light valueForKey:@"lname"];
    self.navigationItem.titleView = _t;
    [_deleteLight setTitle:NSLocalizedString(@"deletelight", nil) forState:UIControlStateNormal];
    _deleteLight.titleLabel.font = [UIFont systemFontOfSize:23];
    UIImageView *delete = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"roomSel.png"]];
    delete.frame=CGRectMake(0, 0, _deleteLight.frame.size.width, _deleteLight.frame.size.height);
    [_deleteLight addSubview:delete];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [self refresh];
}
-(void)lrefresh{
    if(self.view.window!=nil){
        NSMutableArray *lights;
        lights = [_analyse getAllLights];
        int a =[_lp searchLight:_light fromLights:lights];
        if(a!=-1){
            lights = [_analyse getAllConnectedLights];
            int a =[_lp searchLight:_light fromLights:lights];
            if(a==-1){
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"loseConnection",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok",nil)  otherButtonTitles:nil, nil];
                [alert1 show];
                [alert1 setTag:1];
            }
        }
    }
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (IBAction)nameEdit:(id)sender {
    _ltvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightText"];
    _ltvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _ltvc.light =[[NSMutableDictionary alloc]init];
    _ltvc.light = _light;
    [self.navigationController pushViewController:_ltvc animated:YES];
}
- (IBAction)roomEdit:(id)sender {
    _lcrvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightRoom"];
    _lcrvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _lcrvc.light =[[NSMutableDictionary alloc]init];
    _lcrvc.light = _light;
    [self.navigationController pushViewController:_lcrvc animated:YES];
}

-(void)refresh
{
    _light = [_analyse getLight:[_light valueForKey:@"lnameID"]];
    [_lightName setTitle:[_light valueForKey:@"lname"] forState:UIControlStateNormal];
    [_roomname setTitle:[[_analyse getRoom:[_light valueForKey:@"lroom"]]valueForKey:@"rname"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (IBAction)deleteLight:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"deleteLight", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    [alert setTag:2];
    [alert show];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if(alertView.tag==2){
        if(buttonIndex==0){
        }
        else if(buttonIndex==1){
            [self.navigationController popToRootViewControllerAnimated:YES];
            [_analyse deleteLight:_light];
            blePeripheral *bp;
            bp=[_lp searchBLEfromLight:_light];
            if(bp.activePeripheral.state){
                [blead.ble disconnectPeripheral:bp.activePeripheral];
            }
            
        }
    }
}
@end
