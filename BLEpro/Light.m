//
//  Light.m
//  BLEpro
//
//  Created by u on 14-5-21.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "Light.h"


@implementation Light

@dynamic lbreathstate;
@dynamic lcolor;
@dynamic lconfig;
@dynamic lconnect;
@dynamic lgradstate;
@dynamic lID;
@dynamic lname;
@dynamic lnameID;
@dynamic lroom;
@dynamic lrstate;
@dynamic lstate;
@dynamic lwakestate;
@dynamic lwaketime;
@dynamic lbright;

@end
