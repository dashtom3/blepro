//
//  RootViewController.h
//  BLEpro
//
//  Created by u on 14-5-28.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "slideView.h"
#import "upView.h"
#import "tableUpView.h"
#import "ModelAnalyse.h"
#import "BLEproAppDelegate.h"
#import "listView.h"
#import "LightAddViewController.h"
#import "LightSceneViewController.h"
#import "LightBreathViewController.h"
#import "EditRoomViewController.h"
#import "ShakeEffectViewController.h"
#import "LightProvider.h"
#import "effectView.h"
#import "upView.h"
enum {
    // 当前view
    light =0,
    room ,
    effect,
    slide,
    // 当前向左右向上下判断
    nopan=-1,
    toright,
    toupordown,
    //up打开状态
    upViewOff=0,
    upViewOn=1,
};
@interface RootViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

//data
@property(nonatomic) NSMutableDictionary *control;
@property(nonatomic) NSMutableArray *rooms;
//@property(nonatomic) NSMutableArray *lights;
@property(nonatomic) NSMutableArray *connectedlights;
@property(nonatomic) ModelAnalyse *ma;
@property(nonatomic) blePeripheral *bp;
@property(nonatomic) LightProvider *lp;
//control
@property (nonatomic) UILabel *viewTitle;
@property (nonatomic) UIButton *viewRbtn;

//small view
@property (nonatomic) slideView *slideView;
@property (nonatomic) tableUpView *tableUpView;
@property (nonatomic) upView *upView;
//pangesture;
@property (nonatomic) CGPoint panbounds;
@property (nonatomic) CGPoint panlightbounds;
@property (nonatomic) int panstate;
//view
@property(nonatomic) int currentView;
@property (nonatomic) UIView *toplightview;
@property (nonatomic) effectView *effectView;
@property (strong,nonatomic) UITableView *roomView;
@property (nonatomic) listView *lightView;

//push View
@property (weak,nonatomic) LightAddViewController *lavc;
@property (weak,nonatomic) LightSceneViewController *lsvc;
@property (weak,nonatomic) LightBreathViewController *lbvc;
@property (weak,nonatomic) EditRoomViewController *ervc;
@property (weak,nonatomic) ShakeEffectViewController *sevc;


@end
