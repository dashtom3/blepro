//
//  LightChangeRoomViewController.m
//  BLEpro
//
//  Created by u on 14-5-21.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightChangeRoomViewController.h"
#import "allRoomsCell.h"
@interface LightChangeRoomViewController ()

@end

@implementation LightChangeRoomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"room", nil);
    _ma = [[ModelAnalyse alloc]init];
    _rooms = [[NSMutableArray alloc]init];
    _rooms = [_ma getAllRooms];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, 320,self.view.bounds.size.height-64)];
    _tableView.backgroundColor = [UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)64/255 alpha:1.0];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = [UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0];
    _tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.tintColor = [UIColor greenColor];
    UIView *bgv = [[UIView alloc]init];
    [bgv setBackgroundColor:[UIColor clearColor]];
    cell.selectedBackgroundView = bgv;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_rooms count];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_light setObject:[[_rooms objectAtIndex:indexPath.row]valueForKey:@"rID"] forKey:@"lroom"];
    [_ma changeLight:_light];
    [self.navigationController popViewControllerAnimated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    cell.detailTextLabel.text=NSLocalizedString(@"no", nil);
    if([[_light valueForKey:@"lroom"] isEqualToString:[[_rooms objectAtIndex:indexPath.row] valueForKey:@"rID"]]){
        UIImageView *sel=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"select.png"]];
        sel.frame = CGRectMake(270, 29, 40, 41);
        [cell addSubview:sel];
    }
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    cell.textLabel.tintColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont systemFontOfSize:20];
    cell.textLabel.text=[[_rooms objectAtIndex:indexPath.row] valueForKey:@"rname"];
    cell.detailTextLabel.text=@"";
    UIColor * backgroundColor = [UIColor clearColor];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:backgroundColor];
    UIImageView *tableBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tableviewline.png"]];
    tableBg.frame=CGRectMake(cell.frame.origin.x,cell.frame.origin.y+97,cell.frame.size.width,1);
    [cell insertSubview:tableBg atIndex:0];
    return cell;
}


@end
