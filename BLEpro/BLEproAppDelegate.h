//
//  BLEproAppDelegate.h
//  BLEpro
//
//  Created by u on 14-2-12.
//  Copyright (c) 2014年 u. All rights reserved.
//
// 消息通知
//==============================================


#import <UIKit/UIKit.h>
#import "bleCentralManager.h"
#import "ModelAnalyse.h"
#import "MotionNotifacation.h"
#import "LightProvider.h"


@class MotionNotifacation;
@class LightProvider;

@interface BLEproAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@property (strong, nonatomic) bleCentralManager *ble;
@property (strong, nonatomic) UINavigationController *nc;
@property (nonatomic) ModelAnalyse *analyse;
@property (nonatomic) MotionNotifacation *mn;
@property (nonatomic) LightProvider *lp;
@property (nonatomic) NSMutableArray *lights;
@end
