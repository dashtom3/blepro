//
//  LightTimeViewController.m
//  BLEpro
//
//  Created by u on 14-5-21.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightTimeViewController.h"

@interface LightTimeViewController (){
    BLEproAppDelegate *blead;
}

@end

@implementation LightTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
//    _ltimepicker = [[UIPickerView alloc]initWithFrame:CGRectMake(20, 120, 280, 240)];
    _num=-1;
    [_lwakepicker setDelegate:self];
    [_lwakepicker setDataSource:self];
    _lwakepicker.layer.cornerRadius = 6;
    _lwakepicker.layer.masksToBounds = YES;
    [_ltimepicker setDataSource:self];
    [_ltimepicker setDelegate:self];
    _ltimepicker.layer.cornerRadius = 6;
    _ltimepicker.layer.masksToBounds = YES;
    _pickerlabel =[[UIButton alloc]initWithFrame:CGRectMake(220, 160, 70, 50)];
    _pickerlabel.backgroundColor = [UIColor clearColor];
    [_pickerlabel setBackgroundImage:[UIImage imageNamed:@"roomSel.png"] forState:UIControlStateNormal];
    _pickerlabel.titleLabel.textColor=[UIColor whiteColor];
    [_pickerlabel setTitle:NSLocalizedString(@"ok", nil) forState:UIControlStateNormal];
    [_pickerlabel addTarget:self action:@selector(timeselect) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_pickerlabel];
    [self hidePicker];
    
    hours = [[NSMutableArray alloc]init];
    minutes = [[NSMutableArray alloc]init];
    for(int i=0;i<60;i++){
        if(i<24){
            [hours addObject:[NSString stringWithFormat:@"%d",i]];
        }
        [minutes addObject:[NSString stringWithFormat:@"%d",i]];
    }
    states = [[NSArray alloc]initWithObjects:NSLocalizedString(@"oFF", nil),NSLocalizedString(@"oN", nil),NSLocalizedString(@"noSet", nil), nil];
    
    _lp = [[LightProvider alloc]init];
    _da = [[DateAnalyse alloc]init];
    _ma = [[ModelAnalyse alloc]init];
    _timer = [[NSMutableArray alloc]init];
    _bp = [_lp searchBLEfromLight:_light];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];

}
-(void)lrefresh{
    NSMutableArray *lights;
    lights = [_ma getAllConnectedLights];
    int a =[_lp searchLight:_light fromLights:lights];
    if(a==-1){
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"loseConnection",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok",nil)  otherButtonTitles:nil, nil];
        [alert1 show];
        [alert1 setTag:1];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    _light=[_ma getLight:[_light valueForKey:@"lnameID"]];
    _timer=[_ma getTimeOfLight:_light];
    for(int i=0;i<_timer.count;i++){
        [self setText:i WithState:[[[_timer objectAtIndex:i] valueForKey:@"tstate"]intValue] WithDate:[[_timer objectAtIndex:i]valueForKey:@"tdate"]];
    }
    if([[_light valueForKey:@"lwakestate"]isEqualToString:@"1"]){
        _wakeSwitch.on=YES;
    }else{
        _wakeSwitch.on=NO;
    }
    _lwakeSet.text=[_da getHHmmfromdate:[_light valueForKey:@"lwaketime"]];
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
-(void)showPicker{
    _lwakepicker.alpha=1.0;
    _ltimepicker.alpha=1.0;
    _pickerlabel.alpha=1.0;
}
-(void)hidePicker{
    _lwakepicker.alpha=0.0;
    _ltimepicker.alpha=0.0;
    _pickerlabel.alpha=0.0;
}
-(void)timeselect{
    _timer = [_ma getTimeOfLight:_light];
    _light=[_ma getLight:[_light valueForKey:@"lnameID"]];
    if(_num==0){
        NSInteger h=[_lwakepicker selectedRowInComponent:0];
        NSInteger m=[_lwakepicker selectedRowInComponent:1];
        NSDate *date = [_da getDateWithHour:h WithMinute:m];
        if([self judgeTime:date WithTimer:_timer WithLight:_light]){
            [_light setObject:date forKey:@"lwaketime"];
            [_ma changeLight:_light];
            _lwakeSet.text = [_da getHHmmfromdate:date];
            [_bp wakeUpOpen:[_light valueForKey:@"lwaketime"]];
            [_bp methodOpenWithTime:_timer WithLight:_light];
            [self hidePicker];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"conflict", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
            [alert setTag:2];
            [alert show];
        }
    }else{
        NSInteger h=[_ltimepicker selectedRowInComponent:0];
        NSInteger m=[_ltimepicker selectedRowInComponent:1];
        NSDate *date = [_da getDateWithHour:h WithMinute:m];
        NSInteger state=[_ltimepicker selectedRowInComponent:2];
        if([self judgeTime:date WithTimer:_timer WithLight:_light]){
            for(int i=0;i<_timer.count;i++){
                if([[[_timer objectAtIndex:i]valueForKey:@"tnum"]isEqualToString:[NSString stringWithFormat:@"%d",_num-1]]){
                    [[_timer objectAtIndex:i] setObject:date forKey:@"tdate"];
                    [[_timer objectAtIndex:i] setObject:[NSString stringWithFormat:@"%ld",(long)state] forKey:@"tstate"];
                    [_ma changeTime:[_timer objectAtIndex:i]];
                    [self setText:_num-1 WithState:state WithDate:date];
                    if(state!=2){
                        [_bp setTime:[[_timer objectAtIndex:i ] valueForKey:@"tdate"] WithNum:[[_timer objectAtIndex:i ] valueForKey:@"tnum"] WithGrad:[_light valueForKey:@"lgradstate"] WithState:[[_timer objectAtIndex:i ] valueForKey:@"tstate"] WithBright:[_light valueForKey:@"lbright"]];
                    }else{
                        [_bp writeMethodWithN:[[[_timer objectAtIndex:i ] valueForKey:@"tnum"]intValue] s:0 m:0 H:0 d:0 M:0 hy:0 ly:0 t:0 RGB:0 Hlength:0 Llength:0];
                    }
                }
            }
            [_bp methodOpenWithTime:_timer WithLight:_light];
            [self hidePicker];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"conflict", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
            [alert setTag:2];
            [alert show];
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(BOOL)judgeTime:(NSDate *)date WithTimer:(NSMutableArray *)timer WithLight:(NSMutableDictionary *)light{
    for(int i=0;i<timer.count;i++){
        if(![[[timer objectAtIndex:i] valueForKey:@"tstate"]isEqualToString:@"2"]){
            NSDate *tdate = [[timer objectAtIndex:i] valueForKey:@"tdate"];
            if([[_da getHHmmfromdate:tdate]isEqualToString:[_da getHHmmfromdate:date]]){
                if(_num-1!=i){
                    return false;
                }
            }
        }
    }
    NSDate *tdate = [light valueForKey:@"lwaketime"];
    if([[light valueForKey:@"lwakestate"]isEqualToString:@"1"]){
        if([[_da getHHmmfromdate:tdate]isEqualToString:[_da getHHmmfromdate:date]]){
            if(_num!=0){
                return false;
            }
        }
    }
    return true;
}
-(void)setText:(int)number WithState:(NSInteger)state WithDate:(NSDate *)date{
    switch (number) {
        case 0:
        {
            if(state==1){
                _lt1.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oN", nil)];
            }else if(state==0){
                _lt1.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oFF", nil)];
            }else{
                _lt1.text=NSLocalizedString(@"noSet", nil);
            }
        }
            break;
        case 1:
        {
            if(state==1){
                _lt2.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oN", nil)];
            }else if(state==0){
                _lt2.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oFF", nil)];
            }else{
                _lt2.text=NSLocalizedString(@"noSet", nil);
            }
        }
            break;
        case 2:{
            if(state==1){
                _lt3.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oN", nil)];
            }else if(state==0){
                _lt3.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oFF", nil)];
            }else{
                _lt3.text=NSLocalizedString(@"noSet", nil);
            }
        }
            break;
        case 3:{
            if(state==1){
                _lt4.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oN", nil)];
            }else if(state==0){
                _lt4.text=[NSString stringWithFormat:@"%@ %@",[_da getHHmmfromdate:date],NSLocalizedString(@"oFF", nil)];
            }else{
                _lt4.text=NSLocalizedString(@"noSet", nil);
            }
        }
            break;
        default:
            break;
    }
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if(_state == 0){
        return 3;
    }else
        return 2;
}
//返回当前列显示的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component==0){
        return [hours count];
    }else if (component==1){
        return [minutes count];
    }
    return [states count];
}
//设置当前行的内容
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component==0){
        return [NSString stringWithFormat:@"%@ %@",[hours objectAtIndex:row],NSLocalizedString(@"hour", nil)];
    }
    else if(component==1){
        return [NSString stringWithFormat:@"%@ %@",[minutes objectAtIndex:row],NSLocalizedString(@"minute", nil)];
    }
    return [states objectAtIndex:row];
}

- (IBAction)lwakeset:(id)sender {
    _num=0;
    [self showPicker];
}
- (IBAction)lwakeswitch:(id)sender {
    _num=0;
    if([[_light valueForKey:@"lwakestate"]isEqualToString:@"1"]){
        [_light setObject:@"0" forKey:@"lwakestate"];
        [_bp writeMethodWithN:0 s:0 m:0 H:0 d:0 M:0 hy:0 ly:0 t:0 RGB:0 Hlength:0 Llength:0];
        [_ma changeLight:_light];
        _wakeSwitch.on=NO;
    }else{
        if([self judgeTime:[_light valueForKey:@"lwaketime"] WithTimer:_timer WithLight:_light]){
            [_light setObject:@"1" forKey:@"lwakestate"];
            [_bp wakeUpOpen:[_light valueForKey:@"lwaketime"]];
            [_ma changeLight:_light];
            _wakeSwitch.on=YES;
        }else{
            _wakeSwitch.on=NO;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"conflict", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
            [alert setTag:2];
            [alert show];
        }
    }
    [_bp methodOpenWithTime:_timer WithLight:_light];
}
- (IBAction)tset1:(id)sender {
    _num=1;
    [self showPicker];
}
- (IBAction)tset2:(id)sender {
    _num=2;
    [self showPicker];
}
- (IBAction)tset3:(id)sender {
    _num=3;
    [self showPicker];
}
- (IBAction)tset4:(id)sender {
    _num=4;
    [self showPicker];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
