//
//  Control.h
//  BLEpro
//
//  Created by u on 14-6-2.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Control : NSManagedObject

@property (nonatomic, retain) NSString * lnumber;
@property (nonatomic, retain) NSString * lrcontrol;
@property (nonatomic, retain) NSString * lrstate;
@property (nonatomic, retain) NSString * breathalert;

@end
