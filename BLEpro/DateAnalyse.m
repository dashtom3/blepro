//
//  DateAnalyse.m
//  BLEpro
//
//  Created by u on 14-4-24.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "DateAnalyse.h"
#import "blePeripheral.h"
@implementation DateAnalyse
//yyyy-MM-dd EEEE HH:mm:ss a

//小时
-(int)getHHfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH"];
    NSString *DateString=[formatter stringFromDate: date];
    return [DateString intValue];
}
//分钟
-(int)getmmfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"mm"];
    NSString *DateString=[formatter stringFromDate: date];
    return [DateString intValue];
}
//小时：分钟
-(NSString *)getHHmmfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *DateString=[formatter stringFromDate: date];
    return DateString;
}

//秒
-(int)getssfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ss"];
    NSString *DateString=[formatter stringFromDate: date];
    return [DateString intValue];
}
//日
-(int)getddfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    NSString *DateString=[formatter stringFromDate: date];
    return [DateString intValue];
}
//月
-(int)getMMfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    NSString *DateString=[formatter stringFromDate: date];
    return [DateString intValue];
}
//年
-(int)getyyfromdate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *DateString=[formatter stringFromDate: date];
    return [DateString intValue];
}
-(NSDate *)getDateWithHour:(NSInteger)hour WithMinute:(NSInteger)minute{

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(3600*hour+60*minute+57600)];
    return date;
}
@end
