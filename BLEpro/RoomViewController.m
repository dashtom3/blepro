//
//  RoomViewController.m
//  BLEpro
//
//  Created by u on 14-3-4.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "RoomViewController.h"
#import "KZColorPicker.h"
#import "UIColor-Expanded.h"

@interface RoomViewController (){
    BLEproAppDelegate *blead;
}

@property struct rect
{
CGRect frect;
CGRect lrect;
};
@end

@implementation RoomViewController
@synthesize tableView;
@synthesize UPView;
@synthesize uiView;
@synthesize rooms;
@synthesize lights;
@synthesize analyse;
struct rect tv;
struct rect uv;
struct rect sv;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    blead = [[UIApplication sharedApplication]delegate];
    
    
    UIImageView *imv =  [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"slide1.png"]];
    imv.frame = self.view.frame;
    [self.view insertSubview:imv atIndex:0];
    
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"room", nil);
    self.navigationItem.titleView = t;
    
    UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [rightButton setImage:[UIImage imageNamed:@"roomedit.png"]forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(editroom)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
    
    UIButton*leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [leftButton setImage:[UIImage imageNamed:@"slide.png"]forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(swipeRight:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem= leftItem;
    
    //tableView init
    tv.frect = CGRectMake(0, 64, 320, self.view.bounds.size.height-64);
    tv.lrect = CGRectMake(200, 85, 320, 340	);
    uv.frect = CGRectMake(0,0,self.view.bounds.size.width,100);
    uv.lrect = CGRectMake(0,self.navigationController.navigationBar.bounds.size.height+10,self.view.bounds.size.width,100);
    sv.frect = CGRectMake(-250, 0, 250, 500);
    sv.lrect = CGRectMake(0, 0, 250, 500);
    
    if(_state==1){
        tableView = [[UITableView alloc] initWithFrame:tv.lrect];
        uiView = [[slideView alloc]initWithFrame:sv.lrect];
        uiView.location = 2;
        uiView.alpha = 1.0;
        [self swipeLeft:self];
    }
    else{
        tableView = [[UITableView alloc] initWithFrame:tv.frect];
        uiView = [[slideView alloc]initWithFrame:sv.frect];
    }
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    [self.view addSubview:tableView];
    tableView.scrollEnabled = NO;
    tableView.backgroundColor = [UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0];
    tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    
    //upView init
    UPView = [[upView alloc]initWithFrame:uv.frect];
    [UPView initialButton:2];
    [self.view addSubview:UPView];
    UPView.alpha = 0.0;
    UPView.delegate = self;
    
    _tableUpView = [[tableUpView alloc]init];
    _tableUpView.delegate =self;
    _tableUpView.alpha = 0.0;
    //slideView init
    
    [self.view addSubview:uiView];
    uiView.delegate = self;
    //data operation
    analyse = [[ModelAnalyse alloc]init];
    rooms = [[NSMutableArray alloc]init];
    lights = [[NSMutableArray alloc]init];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)lrefresh{
    lights = analyse.getAllLights;
    rooms = analyse.getAllLightsOfEachRoom;
    blePeripheral *bp;
    int k =0;
    for(int i =0;i<lights.count;i++){
        for(bp in blead.ble.blePeripheralArray){
            if([bp.uuidString isEqualToString:[[lights objectAtIndex:i]valueForKey:@"lnameID"]]){
                if(bp.activePeripheral.state==2){
                    k=1;
                }
            }
        }
        if(k==0){
            [[lights objectAtIndex:i]setObject:@"0" forKey:@"lstate"];
        }
        k=0;
    }
    [tableView reloadData];
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    if(UPView.location ==2){
        [self swipeUP:self];
    }
    lights = analyse.getAllLights;
    rooms = analyse.getAllRooms;
    blePeripheral *bp;
    int k =0;
    for(int i =0;i<lights.count;i++){
        for(bp in blead.ble.blePeripheralArray){
            if([bp.uuidString isEqualToString:[[lights objectAtIndex:i]valueForKey:@"lnameID"]]){
                if(bp.activePeripheral.state==2){
                    k=1;
                }
            }
        }
        if(k==0){
             [[lights objectAtIndex:i]setObject:@"0" forKey:@"lstate"];
        }
        k=0;
    }
    [tableView reloadData];
}

-(void)editroom{
    EditRoomViewController *elvc = [self.storyboard instantiateViewControllerWithIdentifier:@"editRoom"];
    //elvc.rooms = [[NSMutableArray alloc]init];
    //elvc.rooms = rooms;
    [self.navigationController pushViewController:elvc animated:YES];
}
-(void)slidetoview:(int)number{
    switch (number) {
        case 1:
        {
            LightViewController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"light"];
            lvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            lvc.state = 1;
            [self.navigationController pushViewController:lvc animated:NO];
        }
            break;
        case 2:
            [self swipeLeft:self];
            break;
        case 3:
        {
            EffectViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"effect"];
            evc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            evc.state = 1;
            [self.navigationController pushViewController:evc animated:NO];
        }
            break;
        default:
            break;
    }
}
-(void)alclose{
    blePeripheral *bp;
    for(bp in blead.ble.blePeripheralArray){
        if(bp.activePeripheral.state==2){
            [bp InstClickOff];
            for(int i=0;i<lights.count;i++){
                if([[[lights objectAtIndex:i]valueForKey:@"lnameID"]isEqualToString:bp.uuidString])
                {
                    [[lights objectAtIndex:i]setObject:@"0" forKey:@"lstate"];
                    [analyse changeLight:[lights objectAtIndex:i]];
                }
            }
        }
    }
    [tableView reloadData];
    [self swipeUP:self];
}
-(void)alopen{
    blePeripheral *bp;
    for(bp in blead.ble.blePeripheralArray){
        if(bp.activePeripheral.state==2){
            //[bp ClickOn];
            for(int i=0;i<lights.count;i++){
                if([[[lights objectAtIndex:i]valueForKey:@"lnameID"]isEqualToString:bp.uuidString])
                {
                    [bp InstClickOn:[[lights objectAtIndex:i] valueForKey:@"lcolor"] WithAlpha:[[[lights objectAtIndex:i] valueForKey:@"lbright"] intValue]];
                    [[lights objectAtIndex:i]setObject:@"1" forKey:@"lstate"];
                    [analyse changeLight:[lights objectAtIndex:i]];
                }
            }
        }
    }
    [tableView reloadData];
    [self swipeUP:self];
}
-(void)roomalopen{
    blePeripheral *bp;
    for(bp in blead.ble.blePeripheralArray){
        if(bp.activePeripheral.state==2){
            //[bp ClickOn];
            for(int i=0;i<lights.count;i++){
                if([[[rooms objectAtIndex:_tableUpView.ipath.row]valueForKey:@"rID"]isEqualToString:[[lights objectAtIndex:i]valueForKey:@"lroom"]]){
                    if([[[lights objectAtIndex:i]valueForKey:@"lnameID"]isEqualToString:bp.uuidString])
                    {
                        [bp InstClickOn:[[lights objectAtIndex:i] valueForKey:@"lcolor"]WithAlpha:[[[lights objectAtIndex:i] valueForKey:@"lbright"] intValue]];
                        [[lights objectAtIndex:i]setObject:@"1" forKey:@"lstate"];
                        [analyse changeLight:[lights objectAtIndex:i]];
                    }
                }
            }
        }
    }
    [self tableView:tableView didSelectRowAtIndexPath:_tableUpView.ipath];
}
-(void)roomalclose{
    blePeripheral *bp;
    for(bp in blead.ble.blePeripheralArray){
        if(bp.activePeripheral.state==2){
            for(int i=0;i<lights.count;i++){
                if([[[rooms objectAtIndex:_tableUpView.ipath.row]valueForKey:@"rID"]isEqualToString:[[lights objectAtIndex:i]valueForKey:@"lroom"]]){
                    if([[[lights objectAtIndex:i]valueForKey:@"lnameID"]isEqualToString:bp.uuidString])
                    {
                        [bp InstClickOff];
                        [[lights objectAtIndex:i]setObject:@"0" forKey:@"lstate"];
                        [analyse changeLight:[lights objectAtIndex:i]];
                    }
                }
            }
        }
    }
    [self tableView:tableView didSelectRowAtIndexPath:_tableUpView.ipath];
}
- (IBAction)swipeDown:(id)sender {
    if (UPView.location == 1  &&uiView.location == 1){
        [UIView beginAnimations:nil context:nil];
        //持续时间
        [UIView setAnimationDuration:1.0];
        //在出动画的时候减缓速度
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //添加动画开始及结束的代理
        [UIView setAnimationDelegate:self];
        [UIView setAnimationWillStartSelector:@selector(viewButton)];
        //[UIView setAnimationDidStopSelector:@selector(viewButton)];
        //动画效果
        UPView.frame = uv.lrect;
        tableView.frame = CGRectMake(0, 150, 320, self.view.bounds.size.height-150);
        [UIView commitAnimations];
        UPView.location = 2;
    }
}
- (IBAction)swipeUP:(id)sender {
    if (UPView.location == 2){
        [UIView beginAnimations:nil context:nil];
        //持续时间
        [UIView setAnimationDuration:1.0];
        //在出动画的时候减缓速度
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //添加动画开始及结束的代理
        [UIView setAnimationDelegate:self];
        [UIView setAnimationWillStartSelector:@selector(hideButton)];
        //[UIView setAnimationDidStopSelector:@selector(hideButton)];
        //动画效果
        UPView.frame = uv.frect;
        tableView.frame = tv.frect;
        [UIView commitAnimations];
        UPView.location = 1;
    }
}
-(void)viewButton{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:1];
    UPView.alpha = 1.0;
    [UIView commitAnimations];
}
-(void)hideButton{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:1];
    UPView.alpha = 0.0;
    [UIView commitAnimations];
}
- (IBAction)swipeLeft:(id)sender {
    if (uiView.location == 2){
        [UIView beginAnimations:nil context:nil];
        //持续时间
        [UIView setAnimationDuration:1.0];
        //在出动画的时候减缓速度
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //添加动画开始及结束的代理
        [UIView setAnimationDelegate:self];
        //[UIView setAnimationWillStartSelector:@selector(begin)];
        //[UIView setAnimationDidStopSelector:@selector(stopUIView)];
        //动画效果
        uiView.frame = sv.frect;
        tableView.frame = tv.frect;
        self.navigationController.navigationBar.alpha = 1.0;
        [UIView commitAnimations];
        uiView.location = 1;
    }
}


- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender {
    if(uiView.location == 1 &&UPView.location == 1){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        tableView.frame = tv.lrect;
        uiView.frame = sv.lrect;
        self.navigationController.navigationBar.alpha = 0.0;
        
        [UIView commitAnimations];
        uiView.location = 2;
    }
}
- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_tableUpView.location == 1){
        _tableUpView.location =2;
        _tableUpView.ipath = indexPath;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        allRoomsCell *cell = (allRoomsCell *)[tableView cellForRowAtIndexPath:indexPath];
        _tableUpView =[[tableUpView alloc]initWithFrame:CGRectMake(0,0,320,100)];
        _tableUpView.delegate = self;
        _tableUpView.alpha=1.0;
        _tableUpView.location =2;
        _tableUpView.ipath = indexPath;
        //[cell insertSubview:_tableUpView atIndex:1];
        [cell.contentView addSubview:_tableUpView];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        _tableUpView.frame = CGRectMake(0, 100, 320, 100);
        _tableUpView.alpha = 1.0;
        [UIView commitAnimations];
    }
    else if(_tableUpView.location == 2){
        
        if(_tableUpView.ipath.row==indexPath.row){
            _tableUpView.location =1;
            _tableUpView.ipath =NULL;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            _tableUpView.frame = CGRectMake(0, 0, 320, 100);
            _tableUpView.alpha = 0.0;
            [UIView commitAnimations];
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else if(_tableUpView.ipath.row!=indexPath.row){
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            _tableUpView.frame = CGRectMake(0, 0, 320, 100);
            _tableUpView.alpha = 0.0;
            [UIView commitAnimations];
            NSArray *mute = [[NSArray alloc]initWithObjects:_tableUpView.ipath,indexPath,nil];
            _tableUpView.ipath =indexPath;
            [tableView reloadRowsAtIndexPaths:mute withRowAnimation:UITableViewRowAnimationAutomatic];
            allRoomsCell *cell = (allRoomsCell *)[tableView cellForRowAtIndexPath:indexPath];
            _tableUpView =[[tableUpView alloc]initWithFrame:CGRectMake(0,0,320,100)];
            _tableUpView.delegate = self;
            _tableUpView.alpha=0.0;
            _tableUpView.location =2;
            _tableUpView.ipath = indexPath;
            [cell.contentView addSubview:_tableUpView];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:1];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            _tableUpView.frame = CGRectMake(0, 100, 320, 100);
            _tableUpView.alpha = 1.0;
            [UIView commitAnimations];
        }
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [rooms count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_tableUpView.location==2 &&(_tableUpView.ipath.row ==indexPath.row)){
        return 200;
    }
    return 100;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    allRoomsCell *cell = (allRoomsCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"allRooms" owner:self options:nil];
        cell = [array objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        int j=0;
        int k=0;
        for(int i=0;i<[lights count];i++){
            if([[[lights objectAtIndex:i]valueForKey:@"lroom"]isEqualToString:[[rooms objectAtIndex:indexPath.row]valueForKey:@"rID"]]){
                if([[[lights objectAtIndex:i]valueForKey:@"lstate"] isEqualToString:@"1"]){
                    j++;
                }
                else if([[[lights objectAtIndex:i]valueForKey:@"lstate"]isEqualToString:@"0"]){
                    k++;
                }
            }
        }
        [[cell onlight]setText:[NSString stringWithFormat:@"%d",j]];
        [[cell offlight]setText:[NSString stringWithFormat:@"%d",k]];
        [[cell rname]setText:[[rooms objectAtIndex:indexPath.row] valueForKey:@"rname"]];
    }
    
    UIColor * backgroundColor = [UIColor clearColor];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.contentView setBackgroundColor:backgroundColor];
    UIImage *bg =[[UIImage alloc]initWithContentsOfFile:[[rooms objectAtIndex:[indexPath row]] valueForKey:@"rpicture"]];
    UIImageView *tableBg = [[UIImageView alloc] initWithImage:bg];
    tableBg.frame=CGRectMake(cell.frame.origin.x,cell.frame.origin.y,cell.frame.size.width,100);
    [cell insertSubview:tableBg atIndex:0];
    if(_tableUpView.ipath.row==indexPath.row&&_tableUpView.ipath!=NULL){
       
    }
    //cell.detailTextLabel.text=[[lights objectAtIndex:[indexPath row]] valueForKey:@"lroom"];
    return cell;
}

@end
