//
//  LightAddViewController.h
//  BLEpro
//
//  Created by u on 14-5-20.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLEproAppDelegate.h"
#import "lightAddView.h"
#import "ModelAnalyse.h"
#import "LightProvider.h"
@interface LightAddViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *lightname;

@property (weak, nonatomic) IBOutlet UIView *nameLight;
@property (nonatomic)NSMutableArray *lights;
@property (nonatomic)lightAddView *ldv;
@property (nonatomic)int addNum;
@property (nonatomic)NSMutableArray *rooms;
@property (nonatomic)ModelAnalyse *ma;
@property (nonatomic)int panJudge;
@property (nonatomic)LightProvider *lp;
@property (nonatomic)UILabel *label;
@property (nonatomic)UIImageView *tablebg;
@property (nonatomic)NSMutableDictionary *room;
@property (nonatomic)blePeripheral *bp;
@end
