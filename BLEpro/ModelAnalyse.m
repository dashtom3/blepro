//
//  ModelAnalyse.m
//  BLEpro
//
//  Created by u on 14-2-27.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "ModelAnalyse.h"

@implementation ModelAnalyse
//@synthesize light;
@synthesize managedObjectContext = _managedObjectContext;

//control database operations
-(NSMutableDictionary *)getAllControl{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Control"];
    NSError *error;
    NSArray *matches = [_managedObjectContext executeFetchRequest:request error:&error];
    NSManagedObjectContext *info ;
    
    //for(info in matches)
    info = [matches firstObject];
    NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
    if([info valueForKey:@"lrcontrol"]==NULL){
        [data setObject:@"0" forKey:@"lrcontrol"];
    }else{
        [data setObject:[info valueForKey:@"lrcontrol"] forKey:@"lrcontrol"];
    }
    if([info valueForKey:@"lrstate"]==NULL){
        [data setObject:@"0" forKey:@"lrstate"];
    }else{
        [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
    }
    if([info valueForKey:@"lnumber"]==NULL){
        [data setObject:@"0" forKey:@"lnumber"];
    }else{
        [data setObject:[info valueForKey:@"lnumber"] forKey:@"lnumber"];
    }
    if([info valueForKey:@"breathalert"]==NULL){
        [data setObject:@"0" forKey:@"breathalert"];
    }else{
        [data setObject:[info valueForKey:@"breathalert"] forKey:@"breathalert"];
    }
    return data;
}
-(void)changeControl:(NSMutableDictionary *)data{
    {
        id delegate = [[UIApplication sharedApplication] delegate];
        _managedObjectContext = [delegate managedObjectContext];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Control"];
        NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
        NSError *error;
        NSManagedObjectContext *info;
        
        if(![arr count])
        {
            Control *contactInfo = (Control *)[NSEntityDescription insertNewObjectForEntityForName:@"Control" inManagedObjectContext:_managedObjectContext];
            [contactInfo setValue:[data valueForKey:@"lrcontrol"] forKeyPath:@"lrcontrol"];
            [contactInfo setValue:[data valueForKey:@"lrstate"] forKeyPath:@"lrstate"];
            [contactInfo setValue:[data valueForKey:@"lnumber"] forKeyPath:@"lnumber"];
            [contactInfo setValue:[data valueForKey:@"breathalert"] forKeyPath:@"breathalert"];
        }else{
            info =[arr firstObject];
            [info setValue:[data valueForKey:@"lrcontrol"]  forKey:@"lrcontrol"];
            [info setValue:[data valueForKey:@"lrstate"]  forKey:@"lrstate"];
            [info setValue:[data valueForKey:@"lnumber"] forKeyPath:@"lnumber"];
            [info setValue:[data valueForKey:@"breathalert"] forKeyPath:@"breathalert"];
        }
        [_managedObjectContext save:&error];
    }
}
//light database operations
//得到所有的灯
-(NSMutableDictionary *)getLight:(NSString *)lnameID{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
    request.predicate = [NSPredicate predicateWithFormat:@"lnameID=%@",lnameID];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    NSManagedObjectContext *info ;
    NSManagedObject *mo;
    info = [arr firstObject];
    mo = [arr firstObject];
    NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
    [data setObject:mo.objectID forKey:@"lID"];
    [data setObject:[info valueForKey:@"lnameID"] forKey:@"lnameID"];
    [data setObject:[info valueForKey:@"lcolor"] forKey:@"lcolor"];
    [data setObject:[info valueForKey:@"lname"] forKey:@"lname"];
    [data setObject:[info valueForKey:@"lroom"] forKey:@"lroom"];
    [data setObject:[info valueForKey:@"lstate"] forKey:@"lstate"];
    [data setObject:[info valueForKey:@"lconnect"] forKey:@"lconnect"];
    [data setObject:[info valueForKey:@"lconfig"] forKey:@"lconfig"];
    [data setObject:[info valueForKey:@"lgradstate"] forKey:@"lgradstate"];
    [data setObject:[info valueForKey:@"lwakestate"] forKey:@"lwakestate"];
    [data setObject:[info valueForKey:@"lwaketime"] forKey:@"lwaketime"];
    [data setObject:[info valueForKey:@"lbreathstate"] forKey:@"lbreathstate"];
    [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
    [data setObject:[info valueForKey:@"lbright"] forKey:@"lbright"];
    return data;
}
-(NSMutableArray *)getAllLights
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
    NSError *error;
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"lroom" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObjects:descriptor, nil];
    NSArray *matches = [_managedObjectContext executeFetchRequest:request error:&error];
    NSManagedObjectContext *info ;
    
    NSMutableArray *datas = [[NSMutableArray alloc]init];
    NSManagedObject *mo;
    //for(info in matches)
    for(int i=0;i<[matches count];i++)
    {
       
        info = [matches objectAtIndex:i];
        mo = [matches objectAtIndex:i];
        NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
        [data setObject:mo.objectID forKey:@"lID"];
        [data setObject:[info valueForKey:@"lnameID"] forKey:@"lnameID"];
        [data setObject:[info valueForKey:@"lcolor"] forKey:@"lcolor"];
        [data setObject:[info valueForKey:@"lname"] forKey:@"lname"];
        [data setObject:[info valueForKey:@"lroom"] forKey:@"lroom"];
        [data setObject:[info valueForKey:@"lstate"] forKey:@"lstate"];
        [data setObject:[info valueForKey:@"lconnect"] forKey:@"lconnect"];
        [data setObject:[info valueForKey:@"lconfig"] forKey:@"lconfig"];
        [data setObject:[info valueForKey:@"lgradstate"] forKey:@"lgradstate"];
        [data setObject:[info valueForKey:@"lwakestate"] forKey:@"lwakestate"];
        [data setObject:[info valueForKey:@"lwaketime"] forKey:@"lwaketime"];
        [data setObject:[info valueForKey:@"lbreathstate"] forKey:@"lbreathstate"];
        [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
        [data setObject:[info valueForKey:@"lbright"] forKey:@"lbright"];
        [datas addObject:data];
    }
    return datas;
}
//得到每个房间的灯的array
-(NSMutableDictionary *)getAllLightsOfSingleRoomByID:(NSString *)rID{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSMutableDictionary *room;
    room =[self getRoom:rID];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
    request.predicate = [NSPredicate predicateWithFormat:@"lroom=%@",[room valueForKey:@"rID"]];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    NSManagedObjectContext *info ;
    NSMutableArray *datas = [[NSMutableArray alloc]init];
    NSManagedObject *mo;
    for(int j=0;j<[arr count];j++)
    {
            
        info = [arr objectAtIndex:j];
        mo = [arr objectAtIndex:j];
        NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
        [data setObject:mo.objectID forKey:@"lID"];
        [data setObject:[info valueForKey:@"lnameID"] forKey:@"lnameID"];
        [data setObject:[info valueForKey:@"lcolor"] forKey:@"lcolor"];
        [data setObject:[info valueForKey:@"lname"] forKey:@"lname"];
        [data setObject:[info valueForKey:@"lroom"] forKey:@"lroom"];
        [data setObject:[info valueForKey:@"lstate"] forKey:@"lstate"];
        [data setObject:[info valueForKey:@"lconnect"] forKey:@"lconnect"];
        [data setObject:[info valueForKey:@"lconfig"] forKey:@"lconfig"];
        [data setObject:[info valueForKey:@"lgradstate"] forKey:@"lgradstate"];
        [data setObject:[info valueForKey:@"lwakestate"] forKey:@"lwakestate"];
        [data setObject:[info valueForKey:@"lwaketime"] forKey:@"lwaketime"];
        [data setObject:[info valueForKey:@"lbreathstate"] forKey:@"lbreathstate"];
        [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
        [data setObject:[info valueForKey:@"lbright"] forKey:@"lbright"];
        [datas addObject:data];
    }
    [room setObject:datas forKey:@"data"];
    return room;
}

//得到每个房间的灯的array
-(NSMutableArray *)getAllLightsOfEachRoom{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSMutableArray *rooms;
    rooms =[self getAllRooms];
    for(int i=0;i<rooms.count;i++){
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
        request.predicate = [NSPredicate predicateWithFormat:@"lroom=%@",[rooms[i] valueForKey:@"rID"]];
        NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
        NSManagedObjectContext *info ;
        NSMutableArray *datas = [[NSMutableArray alloc]init];
        NSManagedObject *mo;
        for(int j=0;j<[arr count];j++)
        {
            
            info = [arr objectAtIndex:j];
            mo = [arr objectAtIndex:j];
            NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
            [data setObject:mo.objectID forKey:@"lID"];
            [data setObject:[info valueForKey:@"lnameID"] forKey:@"lnameID"];
            [data setObject:[info valueForKey:@"lcolor"] forKey:@"lcolor"];
            [data setObject:[info valueForKey:@"lname"] forKey:@"lname"];
            [data setObject:[info valueForKey:@"lroom"] forKey:@"lroom"];
            [data setObject:[info valueForKey:@"lstate"] forKey:@"lstate"];
            [data setObject:[info valueForKey:@"lconnect"] forKey:@"lconnect"];
            [data setObject:[info valueForKey:@"lconfig"] forKey:@"lconfig"];
            [data setObject:[info valueForKey:@"lgradstate"] forKey:@"lgradstate"];
            [data setObject:[info valueForKey:@"lwakestate"] forKey:@"lwakestate"];
            [data setObject:[info valueForKey:@"lwaketime"] forKey:@"lwaketime"];
            [data setObject:[info valueForKey:@"lbreathstate"] forKey:@"lbreathstate"];
            [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
            [data setObject:[info valueForKey:@"lbright"] forKey:@"lbright"];
            [datas addObject:data];
        }
        [rooms[i] setObject:datas forKey:@"data"];
    }
    return rooms;
}
-(NSMutableArray *)getAllConnectedLightsOfEachRoom{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSMutableArray *rooms;
    rooms =[self getAllRooms];
    for(int i=0;i<rooms.count;i++){
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
        request.predicate = [NSPredicate predicateWithFormat:@"lroom=%@",[rooms[i] valueForKey:@"rID"]];
        NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
        
        NSManagedObjectContext *info ;
        NSMutableArray *datas = [[NSMutableArray alloc]init];
        NSManagedObject *mo;
        for(int j=0;j<[arr count];j++)
        {
            info = [arr objectAtIndex:j];
            mo = [arr objectAtIndex:j];
            if([[info valueForKey:@"lconnect"]isEqualToString:@"1"]){
                NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
                [data setObject:mo.objectID forKey:@"lID"];
                [data setObject:[info valueForKey:@"lnameID"] forKey:@"lnameID"];
                [data setObject:[info valueForKey:@"lcolor"] forKey:@"lcolor"];
                [data setObject:[info valueForKey:@"lname"] forKey:@"lname"];
                [data setObject:[info valueForKey:@"lroom"] forKey:@"lroom"];
                [data setObject:[info valueForKey:@"lstate"] forKey:@"lstate"];
                [data setObject:[info valueForKey:@"lconnect"] forKey:@"lconnect"];
                [data setObject:[info valueForKey:@"lconfig"] forKey:@"lconfig"];
                [data setObject:[info valueForKey:@"lgradstate"] forKey:@"lgradstate"];
                [data setObject:[info valueForKey:@"lwakestate"] forKey:@"lwakestate"];
                [data setObject:[info valueForKey:@"lwaketime"] forKey:@"lwaketime"];
                [data setObject:[info valueForKey:@"lbreathstate"] forKey:@"lbreathstate"];
                [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
                [data setObject:[info valueForKey:@"lbright"] forKey:@"lbright"];
                [datas addObject:data];
            }
        }
        [rooms[i] setObject:datas forKey:@"data"];
    }
    return rooms;
}
//得到所有已连接的灯
-(NSMutableArray *)getAllConnectedLights
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
    NSError *error;
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"lroom" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObjects:descriptor, nil];
    NSArray *matches = [_managedObjectContext executeFetchRequest:request error:&error];
    NSManagedObjectContext *info ;
    
    NSMutableArray *datas = [[NSMutableArray alloc]init];
    NSManagedObject *mo;
    //for(info in matches)
    for(int i=0;i<[matches count];i++)
    {
        
        info = [matches objectAtIndex:i];
        if([[info valueForKey:@"lconnect"]isEqualToString:@"1"]){
            mo = [matches objectAtIndex:i];
            NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
            [data setObject:mo.objectID forKey:@"lID"];
            [data setObject:[info valueForKey:@"lnameID"] forKey:@"lnameID"];
            [data setObject:[info valueForKey:@"lcolor"] forKey:@"lcolor"];
            [data setObject:[info valueForKey:@"lname"] forKey:@"lname"];
            [data setObject:[info valueForKey:@"lroom"] forKey:@"lroom"];
            [data setObject:[info valueForKey:@"lstate"] forKey:@"lstate"];
            [data setObject:[info valueForKey:@"lconnect"] forKey:@"lconnect"];
            [data setObject:[info valueForKey:@"lconfig"] forKey:@"lconfig"];
            [data setObject:[info valueForKey:@"lgradstate"] forKey:@"lgradstate"];
            [data setObject:[info valueForKey:@"lwakestate"] forKey:@"lwakestate"];
            [data setObject:[info valueForKey:@"lwaketime"] forKey:@"lwaketime"];
            [data setObject:[info valueForKey:@"lbreathstate"] forKey:@"lbreathstate"];
            [data setObject:[info valueForKey:@"lrstate"] forKey:@"lrstate"];
            [data setObject:[info valueForKey:@"lbright"] forKey:@"lbright"];
            [datas addObject:data];
        }
    }
    return datas;
}
//设置所有灯为未连接
-(void)setNoConnect{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
    NSError *error;
    NSArray *matches = [_managedObjectContext executeFetchRequest:request error:&error];
    NSManagedObject *no ;
    //for(info in matches)
    for(int i=0;i<[matches count];i++)
    {
        no = [matches objectAtIndex:i];
        [no setValue:@"0" forKey:@"lconnect"];
    }
    if(![_managedObjectContext save:&error])
    {
        NSLog(@"不能保存：%@",[error localizedDescription]);
    }
}
//添加当前组的灯到数据库中
-(void)addLights:(NSMutableArray *)lights
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSMutableDictionary *name;
    for(name in lights){
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
        
        request.predicate = [NSPredicate predicateWithFormat:@"lnameID=%@",[name valueForKey:@"lnameID"]];
        NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
        NSError *error;
        //当前数据库中没有该灯
        if(![arr count])
        {
            Light *contactInfo = (Light *)[NSEntityDescription insertNewObjectForEntityForName:@"Light" inManagedObjectContext:_managedObjectContext];
            contactInfo.lID = @"1";
            contactInfo.lnameID =[name valueForKey:@"lnameID"];
            contactInfo.lname = NSLocalizedString(@"newLight", nil);
            contactInfo.lcolor = @"FFFFFF";
            contactInfo.lroom = @"无";
            contactInfo.lstate = @"0";
            contactInfo.lconnect = @"1";
            contactInfo.lconfig = @"0";
            contactInfo.lgradstate = @"0";
            contactInfo.lwakestate = @"0";
            contactInfo.lwaketime = [NSDate dateWithTimeIntervalSince1970:21600];
            contactInfo.lbreathstate = @"0";
            contactInfo.lrstate = @"0";
            contactInfo.lbright = @"255";
        }
        //当前数据库中有该灯
        else {
            NSManagedObjectContext *info;
            if([[[arr firstObject]valueForKey:@"lconfig"]isEqualToString:@"1"]){
                info = [arr firstObject];
                [info setValue:@"1" forKey:@"lconnect"];
            }
            else if([[[arr firstObject]valueForKey:@"lconfig"]isEqualToString:@"0"]){
                info  = [arr firstObject];
                [info setValue:@"1" forKey:@"lconnect"];
            }
        }
        if(![_managedObjectContext save:&error])
        {
            NSLog(@"不能保存：%@",[error localizedDescription]);
        }
    }
}
//灯没有连接
-(void)disconnectLight:(NSString *)uuid{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Light"];
    request.predicate = [NSPredicate predicateWithFormat:@"lnameID=%@",uuid];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    NSError *error;
    if([arr count])
    {
        NSManagedObjectContext *info;
        info  = [arr firstObject];
        [info setValue:@"0" forKey:@"lconnect"];
    }
    if(![_managedObjectContext save:&error])
    {
        NSLog(@"不能保存：%@",[error localizedDescription]);
    }
}
-(void)changeLight:(NSMutableDictionary *)data
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSError *error;
    NSManagedObject *no = [_managedObjectContext objectWithID:[data valueForKey:@"lID"]];
    [no setValue:[data valueForKey:@"lcolor"]  forKey:@"lcolor"];
    [no setValue:[data valueForKey:@"lnameID"]  forKey:@"lnameID"];
    [no setValue:[data valueForKey:@"lname"]  forKey:@"lname"];
    [no setValue:[data valueForKey:@"lroom"]  forKey:@"lroom"];
    [no setValue:[data valueForKey:@"lstate"]  forKey:@"lstate"];
    [no setValue:[data valueForKey:@"lconfig"]  forKey:@"lconfig"];
    [no setValue:[data valueForKey:@"lgradstate"]  forKey:@"lgradstate"];
    [no setValue:[data valueForKey:@"lwakestate"]  forKey:@"lwakestate"];
    [no setValue:[data valueForKey:@"lwaketime"]  forKey:@"lwaketime"];
    [no setValue:[data valueForKey:@"lbreathstate"]  forKey:@"lbreathstate"];
    [no setValue:[data valueForKey:@"lrstate"]  forKey:@"lrstate"];
    [no setValue:[data valueForKey:@"lbright"]  forKey:@"lbright"];
    [no setValue:[data valueForKey:@"lconnect"]  forKey:@"lconnect"];
    [_managedObjectContext save:&error];
}
//移除单个灯
-(void)deleteLight:(NSMutableDictionary *)data
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSError *error;
    NSManagedObject *no = [_managedObjectContext objectWithID:[data valueForKey:@"lID"]];
    [_managedObjectContext deleteObject:no];
    [_managedObjectContext save:&error];
}
//room database operations
-(NSMutableArray *)getAllRooms
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Room"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"rID" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObjects:descriptor, nil];
    NSError *error;
    NSArray *matches = [_managedObjectContext executeFetchRequest:request error:&error];
    NSManagedObjectContext *info ;
    
    NSMutableArray *datas = [[NSMutableArray alloc]init];
    //for(info in matches)
    for(int i=0;i<[matches count];i++)
    {
        
        info = [matches objectAtIndex:i];
        NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
        [data setObject:[info valueForKey:@"rID"] forKey:@"rID"];
        [data setObject:[info valueForKey:@"rname"] forKey:@"rname"];
        [datas addObject:data];
        
    }
    return datas;
}
-(NSMutableDictionary *)getRoom:(NSString *)rID{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Room"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"rID=%@",rID];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    NSManagedObjectContext *info;
    info =[arr firstObject];
    
    NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
    [data setObject:[info valueForKey:@"rID"] forKey:@"rID"];
    [data setObject:[info valueForKey:@"rname"] forKey:@"rname"];
    return data;
}
-(void)addRoom:(NSMutableDictionary *)data
{
    //存储房间
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Room"];
    
    //request.predicate = [NSPredicate predicateWithFormat:@"rID==@max.rID"];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    
    NSError *error;
    //当前有数据
    if([arr count]){
        [data setObject:[NSString stringWithFormat:@"%d",(int)arr.count+1] forKey:@"rID"];
        Room *contactInfo = (Room *)[NSEntityDescription insertNewObjectForEntityForName:@"Room" inManagedObjectContext:_managedObjectContext];
        contactInfo.rID =[data valueForKey:@"rID"];
        contactInfo.rname = [data valueForKey:@"rname"];
    }
    else{
        Room *contactInfo = (Room *)[NSEntityDescription insertNewObjectForEntityForName:@"Room" inManagedObjectContext:_managedObjectContext];
        [data setObject:@"1" forKey:@"rID"];
        contactInfo.rID =@"1";
        contactInfo.rname = [data valueForKey:@"rname"];
    }
    if(![_managedObjectContext save:&error])
    {
        NSLog(@"不能保存：%@",[error localizedDescription]);
    }
    
    NSMutableArray *lights = [[NSMutableArray alloc]initWithArray: [data valueForKey:@"data"]];
    NSMutableDictionary *light;
    for(light in lights){
        //房间的灯
        if([[light valueForKey:@"ljudge"]isEqual:@"1"]){
            [light setValue:[data valueForKey:@"rID"] forKey:@"lroom"];
            [self changeLight:light];
        }
        //不是房间的灯
        
        light = [[NSMutableDictionary alloc]init];
    }
}
-(void)changeLight:(NSMutableDictionary *)data roomName:(NSString *)rid{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSError *error;
    NSManagedObject *no = [_managedObjectContext objectWithID:[data valueForKey:@"lID"]];
    if([[no valueForKey:@"lroom"]isEqualToString:rid])
    {
        [no setValue:[data valueForKey:@"lroom"]  forKey:@"lroom"];
    }
    if(![_managedObjectContext save:&error])
    {
        NSLog(@"不能保存：%@",[error localizedDescription]);
    }
}
-(void)changeRoom:(NSMutableDictionary *)data
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Room"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"rID=%@",[data valueForKey:@"rID"]];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    NSError *error;
    NSManagedObjectContext *info;
    info =[arr firstObject];
    [info setValue:[data valueForKey:@"rname"]  forKey:@"rname"];
    [_managedObjectContext save:&error];
    
    NSMutableArray *lights = [[NSMutableArray alloc]initWithArray: [data valueForKey:@"data"]];
    NSMutableDictionary *light;
    for(light in lights){
        //以前不知道是不是房间的灯，但现在是房间的灯
        if([[light valueForKey:@"ljudge"]isEqual:@"1"]){
            [light setValue:[data valueForKey:@"rID"] forKey:@"lroom"];
            [self changeLight:light];
        }
        //修改以前是房间的灯为“无”
        else{
            [light setValue:@"1" forKey:@"lroom"];
            [self changeLight:light roomName:[data valueForKey:@"rID"]];
        }
        light = [[NSMutableDictionary alloc]init];
    }
}

-(void)deleteRoom:(NSMutableDictionary *)data
{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Room"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"rID=%@",[data valueForKey:@"rID"]];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];

    NSError *error;
    NSManagedObject *no = [arr firstObject];
    [_managedObjectContext deleteObject:no];
    [_managedObjectContext save:&error];
    NSMutableArray *lights = [[NSMutableArray alloc]initWithArray: [data valueForKey:@"data"]];
    NSMutableDictionary *light;
    light = [[NSMutableDictionary alloc]init];
    for(light in lights){
        [light setObject:@"1" forKey:@"lroom"];
        [self changeLight:light roomName:[data valueForKey:@"rID"]];
        light = [[NSMutableDictionary alloc]init];
    }
}
//time
-(void)addTime: (NSMutableDictionary *)data{
    //存储房间
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SetTime"];
    
    //request.predicate = [NSPredicate predicateWithFormat:@"tID==@max.tID"];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    
    NSError *error;
    //当前有数据
    if([arr count]){
        [data setObject:[NSString stringWithFormat:@"%d",(int)arr.count+1] forKey:@"tID"];
        SetTime *contactInfo = (SetTime *)[NSEntityDescription insertNewObjectForEntityForName:@"SetTime" inManagedObjectContext:_managedObjectContext];
        contactInfo.tID =[data valueForKey:@"tID"];
        contactInfo.lID =[data valueForKey:@"lID"];
        contactInfo.tstate = [data valueForKey:@"tstate"];
        contactInfo.tnum = [data valueForKey:@"tnum"];
        contactInfo.tdate = [data valueForKey:@"tdate"];
    }
    else{
        SetTime *contactInfo = (SetTime *)[NSEntityDescription insertNewObjectForEntityForName:@"SetTime" inManagedObjectContext:_managedObjectContext];
        [data setObject:@"1" forKey:@"tID"];
        contactInfo.tID =[data valueForKey:@"tID"];
        contactInfo.lID =[data valueForKey:@"lID"];
        contactInfo.tstate = [data valueForKey:@"tstate"];
        contactInfo.tnum = [data valueForKey:@"tnum"];
        contactInfo.tdate = [data valueForKey:@"tdate"];
    }
    if(![_managedObjectContext save:&error])
    {
        NSLog(@"不能保存：%@",[error localizedDescription]);
    }
}
-(void)changeTime:(NSMutableDictionary *)data{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SetTime"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"tID=%@",[data valueForKey:@"tID"]];
    NSArray *arr=[_managedObjectContext executeFetchRequest:request error:nil];
    NSError *error;
    NSManagedObjectContext *info;
    info =[arr firstObject];
    [info setValue:[data valueForKey:@"tID"]  forKey:@"tID"];
    [info setValue:[data valueForKey:@"lID"]  forKey:@"lID"];
    [info setValue:[data valueForKey:@"tstate"]  forKey:@"tstate"];
    [info setValue:[data valueForKey:@"tnum"]  forKey:@"tnum"];
    [info setValue:[data valueForKey:@"tdate"]  forKey:@"tdate"];
    [_managedObjectContext save:&error];
}
-(NSMutableArray *)getTimeOfLight:(NSMutableDictionary *)light{
    id delegate = [[UIApplication sharedApplication] delegate];
    _managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SetTime"];
    request.predicate = [NSPredicate predicateWithFormat:@"lID=%@",[light valueForKey:@"lnameID"]];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"tnum" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObjects:descriptor, nil];
    NSError *error;
    NSArray *matches = [_managedObjectContext executeFetchRequest:request error:&error];
    NSManagedObjectContext *info ;
    NSMutableArray *datas = [[NSMutableArray alloc]init];
    //for(info in matches)
    if(![matches count]){
        for(int i=0;i<4;i++){
            NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
            [data setObject:[light valueForKey:@"lnameID"] forKey:@"lID"];
            [data setObject:[NSString stringWithFormat:@"%d", i] forKey:@"tnum"];
            [data setObject:[NSString stringWithFormat:@"%d", 2] forKey:@"tstate"];
            [data setObject:[NSDate dateWithTimeIntervalSince1970:21600] forKey:@"tdate"];
            [self addTime:data];
        }
        datas = [self getTimeOfLight:light];
    }else{
        for(int i=0;i<[matches count];i++)
        {
            info = [matches objectAtIndex:i];
            NSMutableDictionary * data = [[NSMutableDictionary alloc]init];
            [data setObject:[info valueForKey:@"tID"] forKey:@"tID"];
            [data setObject:[info valueForKey:@"lID"] forKey:@"lID"];
            [data setObject:[info valueForKey:@"tstate"] forKey:@"tstate"];
            [data setObject:[info valueForKey:@"tnum"] forKey:@"tnum"];
            [data setObject:[info valueForKey:@"tdate"] forKey:@"tdate"];
            [datas addObject:data];
        }
    }
    return datas;
}
@end
