//
//  LightProvider.m
//  BLEpro
//
//  Created by u on 14-4-23.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightProvider.h"
#import "UIColor-Expanded.h"
@implementation LightProvider{
    BLEproAppDelegate *blead;
}
-(id)init{
    self = [super init];
    blead = [[UIApplication sharedApplication]delegate];
    return  self;
}
-(blePeripheral*)searchBLEfromLight:(NSMutableDictionary*)light{
    blePeripheral *bp;
    for(bp in blead.ble.blePeripheralArray){
        if([bp.uuidString isEqualToString:[light valueForKey:@"lnameID"]]){
            return bp;
        }
    }
    return  NULL;
}
-(NSMutableDictionary*)searchLightfromBLE:(blePeripheral*)bp WithArray:(NSMutableArray *)data{
    NSMutableDictionary *light = [[NSMutableDictionary alloc]init];
    for(light in data){
        if([bp.uuidString isEqualToString:[light valueForKey:@"lnameID"]]){
            return light;
        }
    }
    return  NULL;
}
-(int)searchLight:(NSMutableDictionary *)light fromLights:(NSMutableArray *)lights{
    for(int i=0;i<lights.count;i++){
        if([[[lights objectAtIndex:i]valueForKey:@"lnameID"]isEqualToString:[light valueForKey:@"lnameID"]]){
            return i;
        }
    }
    return -1;
}
-(NSMutableDictionary *)colorChange:(NSString *)color{
    UIColor *col = [UIColor colorWithHexString:color];
    int a=(int)(col.red*255);
    if(a<(int)(col.blue*255)){
        a=(int)(col.blue*255);
    }
    if(a<(int)(col.green*255)){
        a=(int)(col.green*255);
    }
    NSMutableDictionary *c = [[NSMutableDictionary alloc]init];
    [c setObject:[NSNumber numberWithInt:(int)(col.red*255)] forKey:@"red"];
    [c setObject:[NSNumber numberWithInt:(int)(col.blue*255)] forKey:@"blue"];
    [c setObject:[NSNumber numberWithInt:(int)(col.green*255)] forKey:@"green"];
    [c setObject:[NSNumber numberWithInt:a] forKey:@"alpha"];
    //(int)(col.red*255) Blue:(int)(col.blue*255) Green:(int)(col.green*255) Alpha:(int)(col.alpha*255)];
    return c;
}

@end
