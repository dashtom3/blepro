//
//  tableUpView.m
//  BLEpro
//
//  Created by u on 14-4-14.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "tableUpView.h"

@implementation tableUpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImage *add = [UIImage imageNamed:@"upbg.jpg"];
        UIImageView *picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = self.frame;
        [self insertSubview:picView atIndex:0];
        _location = 1;
        _button2 = [[UIButton alloc]initWithFrame:CGRectMake(1, 0, self.bounds.size.width/2-2, self.bounds.size.height)];
        _button2.backgroundColor =[UIColor clearColor];
        [_button2 setTitle:NSLocalizedString(@"allClose", nil) forState:UIControlStateNormal];
        [self addSubview:_button2];
        _button3 = [[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width/2, 0, self.bounds.size.width/2-1, self.bounds.size.height)];
        [_button3 setTitle:NSLocalizedString(@"allOpen", nil) forState:UIControlStateNormal];
        _button3.backgroundColor =[UIColor clearColor];
        [self addSubview:_button3];
        add = [UIImage imageNamed:@"off1.png"];
        picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/5-26,self.bounds.size.height/2.5,15,15);
        [self addSubview:picView];
        add = [UIImage imageNamed:@"on1.png"];
        picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/2+36,self.bounds.size.height/2.5,15,15);
        [self addSubview:picView];
        [_button2 addTarget:self action:@selector(allclose:) forControlEvents:UIControlEventTouchUpInside];
        [_button3 addTarget:self action:@selector(allopen:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

-(void)allclose:(id)sender
{
    if ([_delegate respondsToSelector:@selector(roomalclose)]) {
        [_delegate roomalclose];
    }
}
-(void)allopen:(id)sender
{
    if ([_delegate respondsToSelector:@selector(roomalopen)]) {
        [_delegate roomalopen];
    }
}

@end
