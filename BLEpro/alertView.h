//
//  alertView.h
//  BLEpro
//
//  Created by u on 14-6-2.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
@protocol alertViewDelegate
-(void)alertOption;
@end
@interface alertView : UIView
@property ( nonatomic)  UILabel *alert;
@property (nonatomic)  UITextView *warning;
@property (nonatomic)ModelAnalyse *ma;
@property (nonatomic) UIButton *check;
@property (nonatomic)NSMutableDictionary *control;
@property (nonatomic) id delegate;
@end
