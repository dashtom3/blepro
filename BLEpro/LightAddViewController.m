//
//  LightAddViewController.m
//  BLEpro
//
//  Created by u on 14-5-20.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightAddViewController.h"

@interface LightAddViewController (){
    BLEproAppDelegate *blead;
}

@end

@implementation LightAddViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    blead = [[UIApplication sharedApplication]delegate];
    [self refresh];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
    
}
-(void)lrefresh{
    [self refresh];
}
-(void)refresh{
    for(UIView *view in [_lights valueForKey:@"view"]){
        [view removeFromSuperview];
    }
    [_tablebg removeFromSuperview];
    [_label removeFromSuperview];
    for(int i=0;i<_rooms.count;i++){
        UIImageView *view1 = [[_rooms objectAtIndex:i ] valueForKey:@"layer"];
        [view1 removeFromSuperview];
        UILabel *view2 = [[_rooms objectAtIndex:i ] valueForKey:@"labelname"];
        [view2 removeFromSuperview];
    }
    _room = [[NSMutableDictionary alloc]init];
    _addNum=0;
    _panJudge = -1;
    _lights = [[NSMutableArray alloc]init];
    blePeripheral *bp;
    for(bp in blead.ble.blePeripheralArray){
        if(!bp.activePeripheral.state){
            NSMutableDictionary *light = [[NSMutableDictionary alloc]init];
            _ldv = [[lightAddView alloc]initWithFrame:CGRectMake(_addNum%3*320/3,93+_addNum/3*100, 320/3, 100)];
            [_ldv setState:bp.uuidString];
            [light setObject:_ldv forKey:@"view"];
            [light setObject:bp forKey:@"ble"];
            [self.view insertSubview:_ldv atIndex:1];
            [_lights addObject:light];
            _addNum++;
        }
    }
    _nameLight.alpha=0.0;
    _nameLight.layer.cornerRadius = 6;
    _nameLight.layer.masksToBounds = YES;
    
    _label=[[UILabel alloc]initWithFrame:CGRectMake(0,93+((_addNum-1)/3+1)*100+5,320,14)];
    _label.text = @"将灯拖拽至房间来添加灯";
    _label.textAlignment=NSTextAlignmentCenter;
    _label.font = [UIFont systemFontOfSize:14];
    _label.textColor = [UIColor colorWithRed:(float)89/255 green:(float)97/255 blue:(float)115/255 alpha:1.0];
    [self.view insertSubview:_label atIndex:0];
    
    UIImage *bg =[UIImage imageNamed:@"tableviewline.png"];
    _tablebg = [[UIImageView alloc] initWithImage:bg];
    _tablebg.frame=CGRectMake(0,93+((_addNum-1)/3+1)*100+30,320,2);
    [self.view insertSubview:_tablebg atIndex:0];
    
    _lp = [[LightProvider alloc]init];
    _ma = [[ModelAnalyse alloc]init];
    _rooms = [[NSMutableArray alloc]init];
    _rooms = [_ma getAllRooms];
    for(int i=0;i<_rooms.count;i++){
        UIImageView * roomsel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"roomSel.png"]];
        roomsel.frame=CGRectMake(i%2*160+3,((int)i/2)*70+126+((_addNum-1)/3+1)*100,153,70);
        [self.view insertSubview:roomsel atIndex:0];
        [[_rooms objectAtIndex:i]setObject:roomsel forKey:@"layer"];
        
        UILabel * roomname=[[UILabel alloc]initWithFrame:CGRectMake(i%2*160,((int)i/2)*70+126+((_addNum-1)/3+1)*100,160,70)];
        roomname.text = [[_rooms objectAtIndex:i]valueForKey:@"rname"];
        roomname.font = [UIFont systemFontOfSize:22];
        roomname.textAlignment=NSTextAlignmentCenter;
        roomname.textColor = [UIColor colorWithRed:(float)89/255 green:(float)97/255 blue:(float)115/255 alpha:1.0];
        [roomname setBackgroundColor:[UIColor clearColor]];
        [self.view insertSubview:roomname atIndex:0];
        [[_rooms objectAtIndex:i]setObject:roomname forKey:@"labelname"];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    for(int i=0;i< blead.ble.blePeripheralArray.count;i++){
        _bp =[blead.ble.blePeripheralArray objectAtIndex:i];
        if(_bp.activePeripheral.state==0){
            [blead.ble.blePeripheralArray removeObjectAtIndex:i];
        }
    }
    [blead.ble resetScanning];
}
- (IBAction)panEffect:(UIPanGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.view];
    if(_panJudge!=-1&&_nameLight.alpha==0.0){
        [[[_lights objectAtIndex:_panJudge]valueForKey:@"view" ] setCenter:point];
    }
    
    if(recognizer.state == UIGestureRecognizerStateBegan){
        for(int i=0;i<_addNum;i++){
            if(CGRectContainsPoint([[[_lights objectAtIndex:i] valueForKey:@"view"] frame], point)&&_panJudge==-1){
                _panJudge = i;
                [[[_lights objectAtIndex:i] valueForKey:@"view"] setCenter:point];
            }
        }
    }
    if(recognizer.state == UIGestureRecognizerStateEnded){
        if(_panJudge!=-1){
            for(int i=0;i<_rooms.count;i++){
                if(CGRectContainsPoint([[[_rooms objectAtIndex:i]valueForKey:@"layer"] frame], point)){
                    _room = [_rooms objectAtIndex:i];
                    _nameLight.alpha =1.0;
                    ((lightAddView*)[[_lights objectAtIndex:_panJudge] valueForKey:@"view"]).alpha=0.0;
                }
            }
            if(_nameLight.alpha ==0.0){
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.3];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                ((lightAddView *)[[_lights objectAtIndex:_panJudge]valueForKey:@"view"]).frame =CGRectMake(_panJudge%3*320/3,93+_panJudge/3*100, 320/3, 100);
                [UIView commitAnimations];
                _panJudge=-1;
            }
        }
    }

}
- (IBAction)addLightOK:(id)sender {
    if([_lightname.text isEqual:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"notNull", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
        [alert show];
    }else {
        NSMutableDictionary *light ;
        NSMutableArray *alllights;
        alllights = [_ma getAllLights];
        light = [_lp searchLightfromBLE:(blePeripheral *)[[_lights objectAtIndex:_panJudge]valueForKey:@"ble"]  WithArray: alllights];
        [light setObject:((blePeripheral*)[[_lights objectAtIndex:_panJudge]valueForKey:@"ble"]).uuidString forKey:@"lnameID"];
        [light setObject:@"1" forKey:@"lconfig"];
        [light setObject:@"0" forKey:@"lconnect"];
        [light setObject:[_room valueForKey:@"rID"] forKey:@"lroom"];
        [light setObject:_lightname.text forKey:@"lname"];
        [_ma changeLight:light];
        [blead.ble connectPeripheral:((blePeripheral *)[[_lights objectAtIndex:_panJudge]valueForKey:@"ble"]).activePeripheral];
        [self refresh];
        [self endEdit:self];
        [self.view endEditing:YES];
        _panJudge=-1;
    }
}
- (IBAction)addLightCancel:(id)sender {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    ((lightAddView *)[[_lights objectAtIndex:_panJudge]valueForKey:@"view"]).alpha =1.0;
    ((lightAddView *)[[_lights objectAtIndex:_panJudge]valueForKey:@"view"]).frame =CGRectMake(_panJudge%3*320/3,93+_panJudge/3*100, 320/3, 100);
    [UIView commitAnimations];
    _panJudge=-1;
    _nameLight.alpha =0.0;
    
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (IBAction)endEdit:(id)sender {
    [sender resignFirstResponder];
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
