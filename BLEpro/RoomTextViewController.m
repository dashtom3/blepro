//
//  RoomTextViewController.m
//  BLEpro
//
//  Created by u on 14-3-6.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "RoomTextViewController.h"

@interface RoomTextViewController ()

@end

@implementation RoomTextViewController
@synthesize roomText;
@synthesize text;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0]];
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"inputRoom", nil);
    
    self.navigationItem.titleView = t;
    if(![text isEqualToString:NULL])
        roomText.text = text;
	// Do any additional setup after loading the view.
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)endEdit:(id)sender {
    if([roomText.text isEqual:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"notNull", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [sender resignFirstResponder];
    }
}
- (IBAction)saveRoomName:(id)sender {
    if([roomText.text isEqual:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"notNull", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSDictionary *dataDict = [NSDictionary dictionaryWithObject:roomText.text forKey:@"name"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RegisterCompletionNotification" object:@"roomname" userInfo:dataDict];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
