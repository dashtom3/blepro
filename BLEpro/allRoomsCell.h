//
//  allRoomsCell.h
//  BLEpro
//
//  Created by u on 14-3-18.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface allRoomsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *onlight;
@property (weak, nonatomic) IBOutlet UILabel *offlight;
@property (weak, nonatomic) IBOutlet UILabel *rname;
@property (strong, nonatomic) IBOutlet UILabel *disconnectlight;

@end
