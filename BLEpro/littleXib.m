//
//  littleXib.m
//  BLEpro
//
//  Created by u on 14-2-28.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "littleXib.h"
#import "UIColor-Expanded.h"
@implementation littleXib
-(void)awakeFromNib{
    self.backgroundColor = [UIColor clearColor];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
       
        
    }
    return self;
}
-(void)drawRect:(CGRect)rect{
    
    CGContextRef contex = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(contex, 6);
    CGContextSetStrokeColorWithColor(contex, [UIColor colorWithHexString:_rgb].CGColor);
    CGContextBeginPath(contex);
    CGContextAddArc(contex, 10,10, 3, 0, 4*M_PI, 1);
    CGContextStrokePath(contex);
    
//    CGContextSetLineWidth(contex, 1);
//    CGContextSetStrokeColorWithColor(contex, [UIColor whiteColor].CGColor);
//    CGContextBeginPath(contex);
//    CGContextAddArc(contex, 10,10, 6.5, 0, 4*M_PI, 1);
//    CGContextStrokePath(contex);
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.



@end
