//
//  LightProvider.h
//  BLEpro
//
//  Created by u on 14-4-23.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEproAppDelegate.h"
@interface LightProvider : NSObject
-(blePeripheral*)searchBLEfromLight:(NSMutableDictionary*)light;
-(int)searchLight:(NSMutableDictionary *)light fromLights:(NSMutableArray *)lights;
-(NSMutableDictionary*)searchLightfromBLE:(blePeripheral*)bp WithArray:(NSMutableArray *)data;
-(NSMutableDictionary *)colorChange:(NSString *)color;
@end
