//
//  SortData.m
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "SortData.h"

@implementation SortData
//对数据进行排序
-(NSMutableArray *)sortMethod:(NSMutableArray *)data{
    NSSortDescriptor *distanceDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lconfig" ascending:NO];
    NSArray *descriptors = [NSArray arrayWithObjects:distanceDescriptor,nil];
    [data sortUsingDescriptors:descriptors];
    distanceDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lconnect" ascending:NO];
    descriptors = [NSArray arrayWithObjects:distanceDescriptor,nil];
    [data sortUsingDescriptors:descriptors];
    return data;
}
//找出nsarray中key为param的nsarray
-(NSArray *)findFromNSArray:(NSArray *)array WhichKey:(NSString *)key equal:(NSString *)param{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@==%@",key],param];
    return [array filteredArrayUsingPredicate:predicate];
}
@end
