//
//  SortData.h
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SortData : NSObject
-(NSMutableArray *)sortMethod:(NSMutableArray *)data;
@end
