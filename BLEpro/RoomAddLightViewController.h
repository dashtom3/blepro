//
//  RoomAddLightViewController.h
//  BLEpro
//
//  Created by u on 14-3-10.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomAddLightViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic) NSMutableDictionary *room;
@property (nonatomic) NSMutableArray *lights;
@end
