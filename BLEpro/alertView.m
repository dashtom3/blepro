//
//  alertView.m
//  BLEpro
//
//  Created by u on 14-6-2.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "alertView.h"

@implementation alertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _ma =[[ModelAnalyse alloc]init];
        _control = [[NSMutableDictionary alloc]init];
        _control = [_ma getAllControl];
        _check = [[UIButton alloc]initWithFrame:CGRectMake(60, 57, 18, 18)];
        [_check setBackgroundImage:[UIImage imageNamed:@"checkOff.png"] forState:UIControlStateNormal];
        _check.backgroundColor = [UIColor blackColor];
        [_check addTarget:self action:@selector(changeNoAlert:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_check];
        _warning = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, 230, 57)];
        _warning.font = [UIFont systemFontOfSize:16];
        _warning.textAlignment = NSTextAlignmentCenter;
        _warning.editable=NO;
        _warning.scrollEnabled=NO;
        [self addSubview:_warning];
        _alert = [[UILabel alloc]initWithFrame:CGRectMake(90, 57, 90, 16)];
        _alert.textAlignment = NSTextAlignmentLeft;
        _alert.font =[UIFont systemFontOfSize:14];
        [self addSubview:_alert];
        UIImageView *line = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tableviewline.png"]];
        line.frame = CGRectMake(0, 76,320, 1);
        [self addSubview:line];
        UIButton *ok = [[UIButton alloc]initWithFrame:CGRectMake(0, 79, 230, 21)];
        [ok setTitle:NSLocalizedString(@"ok", nil) forState:UIControlStateNormal];
        ok.titleLabel.font = [UIFont systemFontOfSize:15];
        [ok setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [ok addTarget:self action:@selector(alertOk:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:ok];
    }
    return self;
}

- (void)alertOk:(id)sender {
    if ([_delegate respondsToSelector:@selector(alertOption)]) {
        [_delegate alertOption];
    }

}
- (void)changeNoAlert:(id)sender {
    if([[_control valueForKey:@"breathalert"]isEqualToString:@"0"]){
        
        [_check setBackgroundImage:[UIImage imageNamed:@"checkOn.png"] forState:UIControlStateNormal];
        [_control setObject:@"1" forKey:@"breathalert"];
        [_ma changeControl:_control];
    }else{
        [_check setBackgroundImage:[UIImage imageNamed:@"checkOff.png"] forState:UIControlStateNormal];
        [_control setObject:@"0" forKey:@"breathalert"];
        [_ma changeControl:_control];
    }
}

@end
