//
//  LightTimeViewController.h
//  BLEpro
//
//  Created by u on 14-5-21.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
#import "DateAnalyse.h"
#import "BLEproAppDelegate.h"
#import "LightProvider.h"
@interface LightTimeViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>{
    NSMutableArray *hours;
    NSMutableArray *minutes;
    NSArray *states;
}

@property (weak, nonatomic) IBOutlet UISwitch *wakeSwitch;
@property (weak, nonatomic) IBOutlet UILabel *lwakeSet;
@property (weak, nonatomic) IBOutlet UILabel *lt1;
@property (weak, nonatomic) IBOutlet UILabel *lt2;
@property (weak, nonatomic) IBOutlet UILabel *lt3;
@property (weak, nonatomic) IBOutlet UILabel *lt4;
@property (weak, nonatomic) IBOutlet UIButton *l5;
@property (weak, nonatomic) IBOutlet UIButton *l4;
@property (weak, nonatomic) IBOutlet UIButton *l3;
@property (weak, nonatomic) IBOutlet UIButton *l2;
@property (weak, nonatomic) IBOutlet UIButton *l1;
@property (nonatomic) NSMutableDictionary *light;
@property (nonatomic) NSMutableArray *timer;
@property (nonatomic) ModelAnalyse *ma;
@property (weak, nonatomic) IBOutlet UIPickerView *ltimepicker;
@property (weak, nonatomic) IBOutlet UIPickerView *lwakepicker;
@property (nonatomic) UIButton *pickerlabel;
@property (nonatomic) int num;
@property (nonatomic) int state;
@property (nonatomic) DateAnalyse *da;
@property (nonatomic) LightProvider *lp;
@property (nonatomic) blePeripheral *bp;
@end
