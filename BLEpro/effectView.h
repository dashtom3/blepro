//
//  effectView.h
//  BLEpro
//
//  Created by u on 14-5-30.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
@protocol lightEffectViewDelegate
@required
- (void)pushEffectShakeViewController;
@end
@interface effectView : UIView
@property (nonatomic) NSMutableArray *connectedlights;
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) NSMutableDictionary *control;
@property (weak, nonatomic) IBOutlet UISwitch *lightesswitch;
@property (weak, nonatomic) IBOutlet UILabel *lightesnum;
@property (nonatomic) id delegate;

-(void)refresh;
@end
