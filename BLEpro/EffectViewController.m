//
//  EffectViewController.m
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "EffectViewController.h"

@interface EffectViewController ()

@property struct rect
{
CGRect frect;
CGRect lrect;
};
@end


@implementation EffectViewController
struct rect sv;
struct rect lv;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *imv =  [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"slide1.png"]];
    imv.frame = self.view.frame;
    [self.view insertSubview:imv atIndex:0];
    
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"rochEffect",nil);
    self.navigationItem.titleView = t;
    
    UIButton*leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [leftButton setImage:[UIImage imageNamed:@"slide.png"]forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(swipeRight:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem= leftItem;

    //data operation
    _analyse = [[ModelAnalyse alloc]init];
    _lp = [[LightProvider alloc]init];
    _control = [_analyse getAllControl];
    if([[_control valueForKey:@"lrcontrol"]isEqualToString:@"1"]){
        _lcontrol.on = YES;
    }
    else {
        _lcontrol.on = NO;
    }
    sv.frect = CGRectMake(-250, 0, 250, 500);
    sv.lrect = CGRectMake(0, 0, 250, 500);
    lv.frect = CGRectMake(0, 64, 320, self.view.frame.size.height-64);
    lv.lrect = CGRectMake(200, 85, 320, 340);
    
    //slideView init
    _uiView = [[slideView alloc]init];
    [self.view addSubview:_uiView];
    _uiView.delegate = self;
    
    //lightseffectview init
   
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    if(_state==1){
        _uiView.frame = sv.lrect;
        _uiView.location = 2;
        _uiView.alpha = 1.0;
        _view1.frame = lv.lrect;
        [self swipeLeft:self];
        _state=0;
    }
    else{
        _uiView.frame=sv.frect;
        _view1.frame = lv.frect;
    }
    [self lrefresh];
}

-(void)lrefresh{
    _lights = [[NSMutableArray alloc]init];
    _lights = [_analyse getAllConnectedLights];
    int k=0;
    for(int i=0;i<_lights.count;i++){
        if([[[_lights objectAtIndex:i ] valueForKey:@"lrstate"]isEqualToString:@"1"]){
            k++;
        }
    }
    _lnum.text=[NSString stringWithFormat:@"%d  %@",k,NSLocalizedString(@"lrnum", nil)];
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (IBAction)lcontrolchange:(id)sender {
    if([[_control valueForKey:@"lrcontrol"]isEqualToString:@"1"]){
        _lcontrol.on=NO;
        [_control setObject:@"0" forKey:@"lrcontrol"];
        [_analyse changeControl:_control];
    }else{
        _lcontrol.on=YES;
        [_control setObject:@"1" forKey:@"lrcontrol"];
        [_analyse changeControl:_control];
    }
}
- (IBAction)pushRControl:(id)sender {
    _sevc = [self.storyboard instantiateViewControllerWithIdentifier:@"shakeEffect"];
    _sevc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:_sevc animated:NO];
}
	
- (IBAction)swipeLeft:(id)sender {
    if (_uiView.location == 2){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        _uiView.frame = sv.frect;
        _view1.frame=lv.frect;
        self.navigationController.navigationBar.alpha = 1.0;
        [UIView commitAnimations];
        _uiView.location = 1;
    }
}

- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender {
    if(_uiView.location == 1){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        _uiView.frame = sv.lrect;
        _view1.frame=lv.lrect;
        self.navigationController.navigationBar.alpha = 0.0;
        [UIView commitAnimations];
        _uiView.location = 2;
    }
}
-(void)slidetoview:(int)number{
    switch (number) {
        case 1:
        {
            LightViewController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"light"];
            lvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            lvc.state = 1;
            [self.navigationController pushViewController:lvc animated:NO];
        }
            break;
        case 2:
        {
            RoomViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"room"];
            rvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            rvc.state = 1;
            [self.navigationController pushViewController:rvc animated:NO];
        }
            break;
        case 3:
            [self swipeLeft:self];
            break;
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
