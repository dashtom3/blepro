//
//  ModelAnalyse.h
//  BLEpro
//
//  Created by u on 14-2-27.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Light.h"
#import "Room.h"
#import "Control.h"
#import "SetTime.h"
@interface ModelAnalyse : NSObject



@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(NSMutableArray *)getAllLights;
-(void)deleteLight:(NSMutableDictionary *)data;
-(void)changeLight:(NSMutableDictionary *)data;
-(NSMutableArray *)getAllRooms;
-(void)addRoom:(NSMutableDictionary *)data;
-(void)changeRoom:(NSMutableDictionary *)data;
-(void)deleteRoom:(NSMutableDictionary *)data;
-(void)addLights:(NSMutableArray *)lights;
-(void)disconnectLight:(NSString*)uuid;
-(void)setNoConnect;
-(NSMutableArray *)getAllConnectedLights;
-(NSMutableDictionary *)getLight:(NSString *)lnameID;
-(NSMutableDictionary *)getRoom:(NSString *)rID;
-(NSMutableDictionary *)getAllControl;
-(void)changeControl:(NSMutableDictionary *)data;
-(NSMutableArray *)getAllLightsOfEachRoom;
-(NSMutableArray *)getTimeOfLight:(NSMutableDictionary *)light;
-(void)changeTime:(NSMutableDictionary *)data;
-(void)addTime: (NSMutableDictionary *)data;
-(NSMutableArray *)getAllConnectedLightsOfEachRoom;
-(NSMutableDictionary *)getAllLightsOfSingleRoomByID:(NSString *)rID;
@end
