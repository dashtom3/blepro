//
//  ShakeEffectViewController.m
//  BLEpro
//
//  Created by u on 14-5-22.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "ShakeEffectViewController.h"

@interface ShakeEffectViewController ()

@end

@implementation ShakeEffectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _lev = [[lightsEffectView alloc]initWithFrame:CGRectMake(0, 64, 320, self.view.bounds.size.height-64)];
    [_lev refresh];
     [self.view addSubview:_lev];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
    // Do any additional setup after loading the view.
}
-(void)lrefresh{
    [_lev refresh];
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
