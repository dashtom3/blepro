//
//  upView.m
//  BLEpro
//
//  Created by u on 14-3-4.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "upView.h"

@implementation upView
@synthesize label;
@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize state;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0];
            }
    return self;
}
-(void)initialButton:(int)num {
    //灯的下拉页面
    if(num == 1){
        button1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height/2-1)];
        button1.backgroundColor =[UIColor colorWithRed:(float)89/255 green:(float)97/255 blue:(float)116/255 alpha:1.0];
        [button1 setTitle:NSLocalizedString(@"refresh", nil) forState:UIControlStateNormal];
        [button1 addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
        UIImage *add = [UIImage imageNamed:@"refresh.png"];
        UIImageView *picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/2.7,self.bounds.size.height/4-10,16,16);
        [self addSubview:button1];
        [self addSubview:picView];
        button2 = [[UIButton alloc]initWithFrame:CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width/2-1, self.bounds.size.height/2)];
        button2.backgroundColor =[UIColor colorWithRed:(float)89/255 green:(float)97/255 blue:(float)116/255 alpha:1.0];
        [button2 setTitle:NSLocalizedString(@"allClose1", nil) forState:UIControlStateNormal];
        [self addSubview:button2];
        add = [UIImage imageNamed:@"close.png"];
        picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/5-15,self.bounds.size.height/1.7,20,20);
        [self addSubview:picView];
        button3 = [[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width/2, self.bounds.size.height/2, self.bounds.size.width/2, self.bounds.size.height/2)];
        button3.backgroundColor =[UIColor colorWithRed:(float)89/255 green:(float)97/255 blue:(float)116/255 alpha:1.0];
        [button3 setTitle:NSLocalizedString(@"allOpen1", nil) forState:UIControlStateNormal];
        [self addSubview:button3];
        add = [UIImage imageNamed:@"open.png"];
        picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/2+46,self.bounds.size.height/1.7,22,22);
        [self addSubview:picView];
    }
    //房间的下拉页面
    else if (num == 2){
        //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"upbg.png"]];
        UIImage *add = [UIImage imageNamed:@"upbg.jpg"];
        UIImageView *picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = self.frame;
        [self insertSubview:picView atIndex:0];
        button2 = [[UIButton alloc]initWithFrame:CGRectMake(1, 0, self.bounds.size.width/2-2, self.bounds.size.height)];
        button2.backgroundColor =[UIColor clearColor];
        [button2 setTitle:NSLocalizedString(@"allClose", nil) forState:UIControlStateNormal];
        [self addSubview:button2];
        button3 = [[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width/2, 0, self.bounds.size.width/2-1, self.bounds.size.height)];
        [button3 setTitle:NSLocalizedString(@"allOpen", nil) forState:UIControlStateNormal];
        button3.backgroundColor =[UIColor clearColor];
        [self addSubview:button3];
        add = [UIImage imageNamed:@"off1.png"];
        picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/5-26,self.bounds.size.height/2.5,15,15);
        [self addSubview:picView];
        add = [UIImage imageNamed:@"on1.png"];
        picView = [[UIImageView alloc] initWithImage:add];
        picView.frame = CGRectMake(self.bounds.size.width/2+36,self.bounds.size.height/2.5,15,15);
        [self addSubview:picView];
        
    }
    [button2 addTarget:self action:@selector(allclose:) forControlEvents:UIControlEventTouchUpInside];
    [button3 addTarget:self action:@selector(allopen:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)refresh
{
    if ([delegate respondsToSelector:@selector(refreshView)]) {
        [delegate refreshView];
    }
}
-(void)allclose:(id)sender
{
    if ([delegate respondsToSelector:@selector(alclose)]) {
        [delegate alclose];
    }
}
-(void)allopen:(id)sender
{
    if ([delegate respondsToSelector:@selector(alopen)]) {
        [delegate alopen];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
