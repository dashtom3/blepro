//
//  main.m
//  BLEpro
//
//  Created by u on 14-2-12.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLEproAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BLEproAppDelegate class]));
    }
}
