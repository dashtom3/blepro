//
//  tableUpView.h
//  BLEpro
//
//  Created by u on 14-4-14.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol tableUpViewDelegate

@required
- (void) roomalclose;
- (void) roomalopen;

@end
@interface tableUpView : UIView
@property (strong,nonatomic) UIButton *button2;
@property (strong,nonatomic) UIButton *button3;
@property (nonatomic) int location;
@property (nonatomic) NSIndexPath * ipath;
@property (nonatomic) id delegate;
@end
