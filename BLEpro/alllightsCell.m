//
//  alllightsCell.m
//  BLEpro
//
//  Created by u on 14-2-28.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "alllightsCell.h"
#import "UIColor-Expanded.h"

@implementation alllightsCell

@synthesize name;
@synthesize room;
@synthesize round;

-(void)awakeFromNib
{
    
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}


-(void)setLabel:(NSString*)kind{
    UILabel *add = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-10)];
    add.textAlignment = NSTextAlignmentCenter;
    add.textColor = [UIColor whiteColor];
    add.font=[UIFont systemFontOfSize:20];
    add.text = kind;
    [self addSubview:add];
    name.alpha = 0.0;
    room.alpha = 0.0;
    round.alpha = 0.0;
}
@end
