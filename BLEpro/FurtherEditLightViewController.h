//
//  FurtherEditLightViewController.h
//  BLEpro
//
//  Created by u on 14-2-22.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Light.h"
#import "ModelAnalyse.h"
#import "BLEproAppDelegate.h"
#import "LightProvider.h"
#import "LightTextViewController.h"
#import "LightChangeRoomViewController.h"

@interface FurtherEditLightViewController : UIViewController
- (IBAction)deleteLight:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *deleteLight;
-(void)refresh;
@property (nonatomic) IBOutlet UIButton *lightName;
@property (nonatomic) IBOutlet UIButton *roomname;
@property (nonatomic) UILabel *t;
@property (strong,nonatomic) NSMutableDictionary *light;
@property (nonatomic)ModelAnalyse *analyse;
@property (nonatomic)LightProvider *lp;
@property (nonatomic)NSMutableArray *rooms;
@property (weak,nonatomic) LightTextViewController *ltvc;
@property (weak,nonatomic) LightChangeRoomViewController *lcrvc;
@end
