//
//  lightAddView.m
//  BLEpro
//
//  Created by u on 14-5-20.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "lightAddView.h"

@implementation lightAddView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setState:(NSString *)name {
    
    UIImage *add = [UIImage imageNamed:@"loff.png"];
    UIImageView *picView = [[UIImageView alloc] initWithImage:add];
    picView.frame = CGRectMake(125/3,10,30,50);
    [self addSubview:picView];
    
    _lname = [[UILabel alloc]initWithFrame:CGRectMake(70/3, 65, 60, 18)];
    _lname.text = name;
    _lname.textColor = [UIColor whiteColor];
    _lname.textAlignment = NSTextAlignmentCenter;
    _lname.lineBreakMode = NSLineBreakByClipping;
    [self addSubview:_lname];
}


@end
