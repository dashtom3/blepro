//
//  LightColorPickViewController.h
//  BLEpro
//
//  Created by u on 14-2-23.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KZColorPicker.h"
#import "ModelAnalyse.h"
#import "LightProvider.h"
@protocol KZDefaultColorControllerDelegate;
@interface LightColorPickViewController : UIViewController
{
    UIColor *selectedColor;
	id<KZDefaultColorControllerDelegate> _delegate;
}
@property(nonatomic, assign) id<KZDefaultColorControllerDelegate> delegate;
@property(nonatomic, retain) UIColor *selectedColor;
@property(nonatomic) NSMutableDictionary *light;
@property(nonatomic) ModelAnalyse *ma;
@property(nonatomic) LightProvider *lp;
@end

@protocol KZDefaultColorControllerDelegate
- (void) defaultColorController:(LightColorPickViewController *)controller didChangeColor:(UIColor *)color;
@end
