//
//  upView.h
//  BLEpro
//
//  Created by u on 14-3-4.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol upViewDelegate

@required
- (void) alclose;
- (void) alopen;
-(void) refreshView;
@end

@interface upView : UIView

@property (strong,nonatomic) UILabel *label;
@property (strong,nonatomic) UIButton *button1;
@property (strong,nonatomic) UIButton *button2;
@property (strong,nonatomic) UIButton *button3;
@property (nonatomic) int state;
-(void)initialButton:(int)num ;

@property (nonatomic) id delegate;
@end
