//
//  EditRoomViewController.m
//  BLEpro
//
//  Created by u on 14-3-4.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "EditRoomViewController.h"

@interface EditRoomViewController ()
@property(nonatomic) UILabel *addroom;
@end

@implementation EditRoomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0]];
    UILabel *t = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"editRoom", nil);
    self.navigationItem.titleView = t;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,30,30)];
    [rightButton setImage:[UIImage imageNamed:@"roomadd.png"]forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(editroom)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
    
    //data operation
    _rooms = [[NSMutableArray alloc]init];
    _analyse = [[ModelAnalyse alloc]init];
    
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    _rooms = _analyse.getAllRooms;
    [_tableView reloadData];
}
-(void)editroom{
    _fervc = [self.storyboard instantiateViewControllerWithIdentifier:@"roomDetail"];
        _fervc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _fervc.state = newRoom;
    _fervc.room = [[NSMutableDictionary alloc]init];
    [self.navigationController pushViewController:_fervc animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_rooms count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _fervc = [self.storyboard instantiateViewControllerWithIdentifier:@"roomDetail"];
    _fervc.state = editRoom;
    _fervc.room = [[NSMutableDictionary alloc]init];
    _fervc.room = [_rooms objectAtIndex:[indexPath row]];
    _fervc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:_fervc animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    UIColor * backgroundColor = [UIColor clearColor];
    [cell setBackgroundColor:backgroundColor];
    [cell.contentView setBackgroundColor:backgroundColor];
    UIImageView *line = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tableviewline.png"]];
    line.frame = CGRectMake(0, 70, 320, 5);
    [cell addSubview:line];
    UIImageView *edit = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"editcon.png"]];
    edit.frame = CGRectMake(cell.frame.size.width-50,20, 30, 30);
    [cell addSubview:edit];
    cell.textLabel.text =[@"= " stringByAppendingString:[[_rooms objectAtIndex:[indexPath row]] valueForKey:@"rname"]];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

@end
