//
//  lightsEffectView.h
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLEproAppDelegate.h"
#import "ModelAnalyse.h"
#import "lightEffectView.h"


@interface lightsEffectView : UIView
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) lightEffectView *lev;
@property (nonatomic) NSMutableArray *lights;
@property (nonatomic) NSMutableArray *rooms;
@property (nonatomic) UILabel *roomName;
@property (nonatomic) int height;
@property (nonatomic)id delegate;
-(void)refresh;
@end
