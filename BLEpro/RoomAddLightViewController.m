//
//  RoomAddLightViewController.m
//  BLEpro
//
//  Created by u on 14-3-10.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "RoomAddLightViewController.h"
#import "alllightsCell.h"
@interface RoomAddLightViewController ()

- (IBAction)saveSelected:(id)sender;

@end

@implementation RoomAddLightViewController
@synthesize lights;
@synthesize room;
@synthesize tableview;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
       
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"addLight", nil);
    self.navigationItem.titleView = t;
    
    
    tableview.backgroundColor = [UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)64/255 alpha:1.0];
    tableview.separatorColor = [UIColor colorWithRed:(float)30/255 green:(float)30/255 blue:(float)30/255 alpha:1.0];
    
    tableview.allowsMultipleSelectionDuringEditing = YES;
    [tableview setEditing:YES animated:YES];
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.tintColor = [UIColor whiteColor];
    UIView *bgv = [[UIView alloc]init];
    [bgv setBackgroundColor:[UIColor clearColor]];
    cell.selectedBackgroundView = bgv;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[lights objectAtIndex:[indexPath row]] setObject:@"1" forKey:@"ljudge"];
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[lights objectAtIndex:[indexPath row]] setObject:@"0" forKey:@"ljudge"];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [lights count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    if([[[lights objectAtIndex:[indexPath row]] valueForKey:@"ljudge"] isEqual:@"1"]){
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
    }
    cell.textLabel.text =[[lights objectAtIndex:[indexPath row]] valueForKey:@"lname"] ;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

- (IBAction)saveSelected:(id)sender {
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:lights
                                                         forKey:@"selected"];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"RegisterCompletionNotification"
     object:@"selected"
     userInfo:dataDict];
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
