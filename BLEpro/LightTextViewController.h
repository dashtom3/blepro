//
//  LightTextViewController.h
//  BLEpro
//
//  Created by u on 14-2-26.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"

@interface LightTextViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *lightText;
@property(strong,nonatomic) NSString *text;
@property (nonatomic)NSMutableDictionary *light;
@property (nonatomic)ModelAnalyse *ma;
@end
