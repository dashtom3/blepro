//
//  slideView.m
//  BLEpro
//
//  Created by u on 14-3-3.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "slideView.h"
#import "LightViewController.h"
#import "RoomViewController.h"

@implementation slideView
@synthesize label;
@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;
@synthesize location;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        location = 1;
        //self.backgroundColor = [UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0];
        //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"slidebar.png"]];
        self.backgroundColor = [UIColor clearColor];
        label = [[UILabel alloc]initWithFrame:CGRectMake(20, 50, 200, 100)];
        label.textColor = [UIColor whiteColor];
        label.font =[UIFont systemFontOfSize:28];
        label.text = @"AirSmith";
        
        [self addSubview:label];
        button1 = [[UIButton alloc]initWithFrame:CGRectMake(10, 130, 200, 40)];
        [button1 setTitle:NSLocalizedString(@"lightTitle", nil) forState:UIControlStateNormal];
        button1.titleLabel.font =[UIFont systemFontOfSize:20];
        [button1 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [button1 setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [self addSubview:button1];
        UIImage *pic = [UIImage imageNamed:@"light.png"];
        UIImageView *picView = [[UIImageView alloc] initWithImage:pic];
        picView.frame = CGRectMake(20, 140, 15, 20);
        [self addSubview:picView];
        
        button2 = [[UIButton alloc]initWithFrame:CGRectMake(10, 180, 200, 40)];
        [button2 setTitle:NSLocalizedString(@"room", nil) forState:UIControlStateNormal];
        button2.titleLabel.font =[UIFont systemFontOfSize:20];
        [button2 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [button2 setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [self addSubview:button2];
        pic = [UIImage imageNamed:@"room.png"];
        picView = [[UIImageView alloc] initWithImage:pic];
        picView.frame = CGRectMake(20, 190, 20, 20);
        [self addSubview:picView];
        
        button3 = [[UIButton alloc]initWithFrame:CGRectMake(10, 230, 200, 40)];
        [button3 setTitle:NSLocalizedString(@"rochEffect", nil) forState:UIControlStateNormal];
        button3.titleLabel.font =[UIFont systemFontOfSize:20];
        [button3 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [button3 setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [self addSubview:button3];
        pic = [UIImage imageNamed:@"theme.png"];
        picView = [[UIImageView alloc] initWithImage:pic];
        picView.frame = CGRectMake(20, 240, 20, 20);
        [self addSubview:picView];
        
        button4 = [[UIButton alloc]initWithFrame:CGRectMake(10, 280, 200, 40)];
        [button4 setTitle:NSLocalizedString(@"setting", nil) forState:UIControlStateNormal];
        button4.titleLabel.font =[UIFont systemFontOfSize:20];
        [button4 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [button4 setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [self addSubview:button4];
        pic = [UIImage imageNamed:@"setting.png"];
        picView = [[UIImageView alloc] initWithImage:pic];
        picView.frame = CGRectMake(20, 290, 18, 20);
        [self addSubview:picView];
        
//selector
        [button1 addTarget:self action:@selector(lightView:) forControlEvents:UIControlEventTouchUpInside];
        [button2 addTarget:self action:@selector(roomView:) forControlEvents:UIControlEventTouchUpInside];
        [button3 addTarget:self action:@selector(effectView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

-(void)lightView:(id)sender
{
    if ([delegate respondsToSelector:@selector(slidetoview:)]) {
        [delegate slidetoview:0];
    }
}
-(void)roomView:(id)sender
{
    if ([delegate respondsToSelector:@selector(slidetoview:)]) {
        [delegate slidetoview:1];
    }
}
-(void)effectView:(id)sender{
    if ([delegate respondsToSelector:@selector(slidetoview:)]) {
        [delegate slidetoview:2];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
