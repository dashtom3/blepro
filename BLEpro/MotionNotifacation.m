//
//  MotionNotifacation.m
//  BLEpro
//
//  Created by u on 14-5-11.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "MotionNotifacation.h"
#import "UIColor-Expanded.h"

@implementation MotionNotifacation{
    BLEproAppDelegate   *blead;
}
-(id)init{
    self = [super init];
    if (self) {
        blead = [[UIApplication sharedApplication]delegate];
        
        _lights = [[NSMutableArray alloc]init];
        _control = [[NSMutableDictionary alloc]init];
        _lp = [[LightProvider alloc]init];
        _analyse = [[ModelAnalyse alloc]init];
        _da = [[DateAnalyse alloc]init];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(lrStateChange)
                                                     name:@"Lrstatechange"
                                                   object:nil];
        nCBCentralStateChange
        nCBPeripheralStateChange
    }
    return self;
}
-(void)lrStateChange{
    _lights = [_analyse getAllConnectedLights];
    _control = [_analyse getAllControl];
    if([[_control valueForKey:@"lrcontrol"]isEqualToString:@"1"]){
        if([[_control valueForKey:@"lrstate"]isEqualToString:@"1"]){
            [_control setObject:@"0" forKey:@"lrstate"];
            [_analyse changeControl:_control];
            for(int i=0;i<_lights.count;i++){
                if([[[_lights objectAtIndex:i]valueForKey:@"lrstate"]isEqualToString:@"1"]){
                    [[_lights objectAtIndex:i]setObject:@"0" forKey:@"lstate"];
                    [_analyse changeLight:[_lights objectAtIndex:i]];
                    if([[[_lights objectAtIndex:i]valueForKey:@"lgradstate"]isEqualToString:@"0"])
                    {
                        [[_lp searchBLEfromLight:[_lights objectAtIndex:i]] InstClickOff];
                    }else{
                        [[_lp searchBLEfromLight:[_lights objectAtIndex:i]] GradualClickOff:[[_lights objectAtIndex:i]valueForKey:@"lcolor"] WithAlpha:[[[_lights objectAtIndex:i]valueForKey:@"lbright"] intValue]];
                    }
                }
            }
        }
        else if([[_control valueForKey:@"lrstate"]isEqualToString:@"0"]){
            [_control setObject:@"1" forKey:@"lrstate"];
            [_analyse changeControl:_control];
            for(int i=0;i<_lights.count;i++){
                if([[[_lights objectAtIndex:i]valueForKey:@"lrstate"]isEqualToString:@"1"]){
                    [[_lights objectAtIndex:i]setObject:@"1" forKey:@"lstate"];
                    [_analyse changeLight:[_lights objectAtIndex:i]];
                    if([[[_lights objectAtIndex:i]valueForKey:@"lgradstate"]isEqualToString:@"0"])
                    {[[_lp searchBLEfromLight:[_lights objectAtIndex:i]] InstClickOn:[[_lights objectAtIndex:i]valueForKey:@"lcolor"]WithAlpha:[[[_lights objectAtIndex:i]valueForKey:@"lbright"] intValue]];
                    }
                    else{
                        [[_lp searchBLEfromLight:[_lights objectAtIndex:i]] GradualClickOn:[[_lights objectAtIndex:i]valueForKey:@"lcolor"]WithAlpha:[[[_lights objectAtIndex:i]valueForKey:@"lbright"] intValue]];
                    }
                }
            }
        }
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"lrefresh"
     object:nil
     userInfo:nil];
}
#pragma mark - 通知回调函数
-(void)CBCentralStateChange{
    blePeripheral *bp=blead.ble.bp;
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateCentralManagerPoweredOff){
        [_analyse setNoConnect];
    }
    //周边断开
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateDisconnectPeripheral){
        //连接超时返回该状态，已连接蓝牙设备失去连接
        [_analyse disconnectLight:bp.uuidString];
        [blead.ble resetScanning];
    }
    //发现周边
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateDiscoverPeripheral){
        NSMutableArray *uuid = [[NSMutableArray alloc]init];
        NSMutableDictionary *bpuuid= [[NSMutableDictionary alloc]init];
        [bpuuid setObject:bp.uuidString forKey:@"lnameID"];
        [uuid addObject:bpuuid];
        [_analyse addLights:uuid];
    }
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateConnectPeripheral){
        _lights=_analyse.getAllLights;
        NSMutableDictionary *light=[_lp searchLightfromBLE:bp WithArray:_lights];
        [light setObject:@"1" forKey:@"lconnect"];
        [_analyse changeLight:light];
    }
    _lights=_analyse.getAllLights;
    NSMutableDictionary *light;
    //连接数据库中显示为连接并且配置过的灯

    for(light in _lights){
        if([[light valueForKey:@"lconnect"]isEqualToString:@"1"]&&[[light valueForKey:@"lconfig"]isEqualToString:@"1"]){
            blePeripheral *bp;
            bp=[_lp searchBLEfromLight:light];
            if (!bp.activePeripheral.state) {
                [blead.ble connectPeripheral:bp.activePeripheral];
            }
        }
    }
    [self CBUpdataShowStringBuffer];
}
-(void)CBPeripheralStateChange:(NSNotification*)notification{
    NSDictionary *data=[notification userInfo];
    NSPredicate *predicate;
    blePeripheral *bp=[data valueForKey:@"bleperipheral"];
    NSMutableDictionary *li;
    _lights = [_analyse getAllLights];
    if(bp.currentPeripheralState ==blePeripheralDelegateStateDiscoverCharacteristics){
        predicate=[NSPredicate predicateWithFormat:@"lnameID==%@",bp.uuidString];
        li = [[_lights filteredArrayUsingPredicate:predicate]firstObject];
        Byte *test = (Byte *)[bp.DataCharateristicUUID2.value bytes];
        if(test!=NULL){
            UIColor *color = [UIColor colorWithRed:(int)test[1]/255 green:(int)test[2]/255 blue:(int)test[3]/255 alpha:(int)test[0]/255];
            if(![color.hexStringFromColor isEqualToString:@"00000000"]){
                [li setObject:color.hexStringFromColor forKey:@"lcolor"];
                [li setObject:[NSString stringWithFormat:@"%d",test[0]] forKey:@"lbright"];
            }else{
                
            }
            if((int)test[0]!=0){
                [li setObject:[NSString stringWithFormat:@"%d",1] forKey:@"lstate"];
            }else{
                [li setObject:[NSString stringWithFormat:@"%d",0] forKey:@"lstate"];
            }
        }
        [_analyse changeLight:li];
        if(bp.TimeCharateristicUUID5!=nil){
            [bp readTimeProperties];
        }
    }
    if(bp.currentPeripheralState ==blePeripheralDelegateStateUpdateCharacteristics){
        predicate=[NSPredicate predicateWithFormat:@"lnameID==%@",bp.uuidString];
        li = [[_lights filteredArrayUsingPredicate:predicate]firstObject];
        Byte *test = (Byte *)[bp.TimeCharateristicUUID2.value bytes];
        NSLog(@"%hhu",test[0]);
        if(test!=NULL){
            if(test[2]==255&&test[3]==255){
                //
            }else if(test[4]==255){
                if(test[0]>=16){
                    _timer = [_analyse getTimeOfLight:li];
                    NSDate *date = [_da getDateWithHour:test[3] WithMinute:test[2]];
                    NSMutableDictionary *time;
                    time = [_timer objectAtIndex:(test[0]-16)];
                    [time setObject:date forKey:@"tdate"];
                    [time setObject:[NSString stringWithFormat:@"%d",(int)(test[9]/255)] forKey:@"tstate"];
                    [_analyse changeTime:time];
                }else if(test[0]==8){
                    
                    NSDate *date = [_da getDateWithHour:test[3] WithMinute:test[2]];
                    [li setObject:date forKey:@"lwaketime"];
                }
                [_analyse changeLight:li];
            }
        }
    }
    if(bp.currentPeripheralState ==blePeripheralDelegateStateUpdate2Characteristics){
        predicate=[NSPredicate predicateWithFormat:@"lnameID==%@",bp.uuidString];
        li = [[_lights filteredArrayUsingPredicate:predicate]firstObject];
        Byte *test = (Byte *)[bp.TimeCharateristicUUID3.value bytes];
        if(test!=NULL){
            //定时设置没有全用。所以判断呼吸灯开启没有时判断定时前面几位从而判断当前晨醒的开启与否
            if(test[3]<16){
                if(test[2]==0){
                    [li setObject:@"0" forKey:@"lwakestate"];
                }else{
                    [li setObject:@"1" forKey:@"lwakestate"];
                }
                [li setObject:@"0" forKey:@"lbreathstate"];
                NSLog(@"lwakestate %@",[li valueForKey:@"lwakestate"]);
            }else{
                [li setObject:@"1" forKey:@"lbreathstate"];
            }
        }
        [_analyse changeLight:li];
    }
    [self CBUpdataShowStringBuffer];
}

-(void)CBUpdataShowStringBuffer{
    // 未在编辑模式下更新显示
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"lrefresh"
     object:nil
     userInfo:nil];
}

@end
