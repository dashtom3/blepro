//
//  lightEffectView.h
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
#import "LightProvider.h"



@interface lightEffectView : UIView
@property (nonatomic) NSMutableDictionary *light;
@property (nonatomic) UIButton *btn;
@property (nonatomic) UILabel *lname;
@property (nonatomic) UIImageView *sel;
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) LightProvider *lp;
@property (nonatomic) id  delegate;
-(void)setState;
@end
