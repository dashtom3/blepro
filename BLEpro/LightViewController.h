//
//  LightViewController.h
//  BLEpro
//
//  Created by u on 14-3-3.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "slideView.h"
#import "RoomViewController.h"
#import "EffectViewController.h"
#import "LightSceneViewController.h"
#import "BLEproAppDelegate.h"
#import "MotionNotifacation.h"
#import "listView.h"
#import "LightAddViewController.h"
#import "LightBreathViewController.h"

@interface LightViewController : UIViewController
@property (strong,nonatomic) slideView *uiView;
@property(nonatomic) int state;
@property (nonatomic) MotionNotifacation *mn;
@property (nonatomic) listView *lv;
@property (weak,nonatomic) LightAddViewController *lavc;
@property (weak,nonatomic) LightSceneViewController *lsvc;
@property (weak,nonatomic) LightBreathViewController *lbvc;
@end
