//
//  timeUpView.m
//  BLEpro
//
//  Created by u on 14-4-25.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "timeUpView.h"

@implementation timeUpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)64/255 alpha:1.0]];
        UIImageView *line = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tableviewline.png"]];
        line.frame = CGRectMake(0, 0, 320, 5);
        [self addSubview:line];
        _wakeUpDate = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 60, 320, 240)];
        // 设置时区
        //[_wakeUpDate setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [_wakeUpDate setTintColor:[UIColor whiteColor]];
        [_wakeUpDate setDatePickerMode:UIDatePickerModeCountDownTimer];
        // 当值发生改变的时候调用的方法
        //[_wakeUpDate addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_wakeUpDate];
        
        _btn1 = [[UIButton alloc]initWithFrame:CGRectMake(1, 0, 160, 60)];
        _btn1.backgroundColor =[UIColor clearColor];
        [_btn1 setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
        [self addSubview:_btn1];
        _btn2 = [[UIButton alloc]initWithFrame:CGRectMake(161, 0, 159, 60)];
        [_btn2 setTitle:NSLocalizedString(@"ok", nil) forState:UIControlStateNormal];
        _btn2.backgroundColor =[UIColor clearColor];
        [self addSubview:_btn2];
        [_btn1 addTarget:self action:@selector(timeCancel:) forControlEvents:UIControlEventTouchUpInside];
        [_btn2 addTarget:self action:@selector(timeOk:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)timeOk:(id)sender{
    if ([_delegate respondsToSelector:@selector(timeSet)]) {
        _date = [_wakeUpDate date];
        [_delegate timeSet];
    }
}
-(void)timeCancel:(id)sender{
    if ([_delegate respondsToSelector:@selector(timeSet)]) {
        [_delegate timeSet];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
