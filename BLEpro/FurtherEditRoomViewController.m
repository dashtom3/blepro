//
//  FurtherEditRoomViewController.m
//  BLEpro
//
//  Created by u on 14-3-5.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "FurtherEditRoomViewController.h"

@interface FurtherEditRoomViewController ()
@property(strong,nonatomic) UILabel *addlight;
@end

@implementation FurtherEditRoomViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    _t.font = [UIFont systemFontOfSize:19];
    _t.textColor = [UIColor whiteColor];
    _t.backgroundColor = [UIColor clearColor];
    _t.textAlignment = NSTextAlignmentCenter;
    _t.text = NSLocalizedString(@"newRoom", nil);
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if(_state == editRoom)
    {
        _t.text = [_room valueForKey:@"rname"];
    }
    self.navigationItem.titleView = _t;
    //imageView = [[UIImageView alloc]init];
    
    //_tableView.scrollEnabled = NO;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    //图片读取初始化
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerCompletion:)
                                                 name:@"RegisterCompletionNotification"
                                               object:nil];
    _ma = [[ModelAnalyse alloc]init];
    _lights = [[NSMutableArray alloc]init];
    _lights =[_ma getAllLights];
    _selectedlights = [[NSMutableArray alloc]init];
    for (int i = 0; i<_lights.count; i++) {
        if([[_room valueForKey:@"rID"]isEqualToString:[[_lights objectAtIndex:i]valueForKey:@"lroom"]])
        {
            [_selectedlights addObject:[_lights objectAtIndex:i]];
            [[_lights objectAtIndex:i] setObject:@"1" forKey:@"ljudge"];
        }
        else{
            [[_lights objectAtIndex:i] setObject:@"0" forKey:@"ljudge"];
        }
    }
    //data operation
    if(_state == editRoom){
        [_rname setTitle:[_room valueForKey:@"rname"] forState:UIControlStateNormal];
    }
    else{
        [_rname setTitle:NSLocalizedString(@"newRoom", nil) forState:UIControlStateNormal];
        [_room setObject:NSLocalizedString(@"newRoom", nil) forKey:@"rname"];
    }
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)registerCompletion:(NSNotification*)notification {
    //接受notification的userInfo，可以把参数存进此变量
    NSDictionary *theData = [notification userInfo];
    if([[notification object] isEqual:@"roomname"])
    {
        NSString *name  = [theData objectForKey:@"name"];
        [_room setObject:name forKey:@"rname"];
        [_rname setTitle:[_room valueForKey:@"rname"] forState:UIControlStateNormal];
        _t.text=name;
    }
    if([[notification object] isEqual:@"selected"])
    {
        NSMutableArray *data  = [theData objectForKey:@"selected"];
        _selectedlights = [[NSMutableArray alloc]init];
        _lights = data;
        for(int i=0;i<_lights.count;i++){
            if([[[_lights objectAtIndex:i] valueForKey:@"ljudge"] isEqual:@"1"])
            {
                [_selectedlights addObject:[_lights objectAtIndex:i]];
            }
        }
    }
    [_tableView reloadData];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showText2"]){
        
        if([segue.destinationViewController isKindOfClass:[RoomTextViewController class]]){
            _rtvc = (RoomTextViewController *)segue.destinationViewController ;
            if(_state == editRoom)
                _rtvc.text = [_room valueForKey:@"rname"];
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //点击删除房间
    if(_state == 1)
    {
        if([indexPath row]==[_selectedlights count]+1){
            if([[_room valueForKey:@"rID"]isEqualToString:@"1"]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Default", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil)otherButtonTitles:nil, nil];
                [alert setTag:1];
                [alert show];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"deleteRoom", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
                [alert setTag:2];
                [alert show];
            }
        }
    }
    //点击添加灯
    if([_selectedlights count]== [indexPath row]){
        _ralvc = [self.storyboard instantiateViewControllerWithIdentifier:@"roomAddLight"];
        _ralvc.lights = [[NSMutableArray alloc]init];
        _ralvc.lights = _lights;
        [self.navigationController pushViewController:_ralvc animated:YES];
        if([indexPath row] != 0){
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==2){
        if(buttonIndex==0){
        }
        else if(buttonIndex==1){
            [_room setObject:_selectedlights forKey:@"data"];
            [_ma deleteRoom:_room];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_state==editRoom)
    {
        return [_selectedlights count]+2;
    }
    else{
        return [_selectedlights count]+1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    alllightsCell *cell = (alllightsCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"allLights" owner:self options:nil];
        cell = [array objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        UIColor * backgroundColor = [UIColor clearColor];
        [cell setBackgroundColor:backgroundColor];
        [cell.contentView setBackgroundColor:backgroundColor];
    }
    
    if([indexPath row]>=[_selectedlights count]){
        if([indexPath row]>[_selectedlights count]){
            [cell setLabel:NSLocalizedString(@"deleteroom", nil)];
            UIImageView *line = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tableviewline.png"]];
            line.frame = CGRectMake(0, 70, 320, 1);
            [cell addSubview:line ];
            cell.imageView.alpha=0.0;
        }
        else{
            [cell setLabel:NSLocalizedString(@"addLight", nil)];
            if(_state==newRoom){
                UIImageView *line = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tableviewline.png"]];
                line.frame = CGRectMake(0, 70, 320, 1);
                [cell addSubview:line ];
                cell.imageView.alpha=0.0;
            }
        }
    }
    else{
        [[cell name] setText:[[_selectedlights objectAtIndex:[indexPath row]] valueForKey:@"lname"]];
        [[cell room] setText:[_room valueForKey:@"rname"]];
        cell.round.rgb =[[_selectedlights objectAtIndex:[indexPath row]] valueForKey:@"lcolor"];
    }
    return cell;
}
- (IBAction)saveroom:(id)sender {
    if([_room valueForKey:@"rname"]==NULL){
        
    }
    //更新
    if(_state == editRoom){
        [_room setObject:_lights forKey:@"data"];
        _ma = [[ModelAnalyse alloc]init];
        [_ma changeRoom:_room];
        [self.navigationController popViewControllerAnimated:YES];
    }
    //新建
    if(_state == newRoom)
    {
        [_room setObject:_lights forKey:@"data"];
        _ma = [[ModelAnalyse alloc]init];
        [_ma addRoom:_room];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
