//
//  DateAnalyse.h
//  BLEpro
//
//  Created by u on 14-4-24.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateAnalyse : NSObject
-(int)getHHfromdate:(NSDate *)date;
-(int)getmmfromdate:(NSDate *)date;
-(int)getssfromdate:(NSDate *)date;
-(int)getddfromdate:(NSDate *)date;
-(int)getMMfromdate:(NSDate *)date;
-(int)getyyfromdate:(NSDate *)date;
-(NSString *)getHHmmfromdate:(NSDate *)date;
-(NSDate *)getDateWithHour:(NSInteger)hour WithMinute:(NSInteger)minute;
@end
