//
//  slideView.h
//  BLEpro
//
//  Created by u on 14-3-3.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol slideViewDelegate

@required
- (void) slidetoview:(int)number;

@end

@interface slideView : UIView
@property (strong,nonatomic) UILabel *label;
@property (strong,nonatomic) UIButton *button1;
@property (strong,nonatomic) UIButton *button2;
@property (strong,nonatomic) UIButton *button3;
@property (strong,nonatomic) UIButton *button4;
@property (nonatomic) int location;
@property (nonatomic) id  delegate;

@end
