//
//  LightBreathViewController.h
//  BLEpro
//
//  Created by u on 14-5-22.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLEproAppDelegate.h"
#import "ModelAnalyse.h"
#import "LightProvider.h"

@interface LightBreathViewController : UIViewController
@property (weak,nonatomic)blePeripheral *bp;
@property (weak, nonatomic) IBOutlet UILabel *lightname;
@property (weak, nonatomic) IBOutlet UILabel *roomname;
@property (nonatomic)NSMutableDictionary *light;
@property (nonatomic)ModelAnalyse *ma;
@property (nonatomic)NSMutableArray *timer;
@property (nonatomic)LightProvider *lp;
@property (nonatomic) NSMutableDictionary *room;
@end
