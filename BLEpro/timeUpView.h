//
//  timeUpView.h
//  BLEpro
//
//  Created by u on 14-4-25.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol timeUpViewDelegate

@required
- (void) timeSet;
@end

@interface timeUpView : UIView
@property (nonatomic) UIDatePicker *wakeUpDate;
@property (nonatomic) UIButton *btn1;
@property (nonatomic) UIButton *btn2;
@property (nonatomic) NSDate *date;
@property (nonatomic) id delegate;
@end
